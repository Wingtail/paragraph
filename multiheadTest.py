import tensorflow as tf
import numpy as np
import math

x = np.array([[[1.0,2.0,3.0,4.0,0.0,0.0,0.0,5.0],
              [1.0,6.0,3.0,4.0,7.0,0.0,0.0,5.0],
              [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],
              [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]],

              [[1.0,2.0,3.0,4.0,8.0,9.0,0.0,5.0],
              [1.0,6.0,3.0,4.0,7.0,0.0,0.0,5.0],
              [1.0,2.0,0.0,0.0,0.0,0.0,0.0,0.0],
              [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]]]
)


def split_heads(x):
    batch_size = tf.shape(x)[0]
    length = tf.shape(x)[1]

    # Calculate depth of last dimension after it has been split.
    depth = 2

    # Split the last dimension
    x = tf.reshape(x, [batch_size, length, 4, depth])

    # Transpose the result
    return tf.transpose(x, [0, 2, 1, 3])



wordDimVals = [10000**(2*k/float(8)) for k in range(8)]

b = tf.reduce_sum(tf.math.abs(x), axis=2)
c = tf.cast(tf.math.logical_not(tf.math.equal(b,0.0)), dtype=tf.float32) #0 -> padding, 1 -> not padding
posMask = tf.expand_dims(c, axis=-1)

posMat = np.zeros((x.shape[1], x.shape[2]), dtype=np.float32)
for i in range(x.shape[1]):
    for j in range(x.shape[2]):
        if(j%2==0):
            posMat[i][j] = math.sin(i/float(wordDimVals[j]))
        else:
            posMat[i][j] = math.cos(i/float(wordDimVals[j]))

posMat *= posMask

q = tf.layers.dense(x, 8, use_bias = False)
k = tf.layers.dense(x, 8, use_bias = False)
v = tf.layers.dense(x, 8, use_bias = False)

b = tf.reduce_sum(tf.math.abs(x), axis=2)
c = tf.cast(tf.math.equal(b,0.0), dtype=tf.float64) #1 -> padding, 0 -> not padding
mask = tf.expand_dims(c, axis=1)

q = split_heads(q)
k = split_heads(k)
v = split_heads(v)

depth = (8 / 4)
q *= depth ** -0.5

logits = tf.matmul(q, k, transpose_b=True)
logits += (tf.expand_dims(mask,1) * -1e9)
logits = tf.nn.softmax(logits)
init = tf.global_variables_initializer()

adding = x+(tf.reshape(mask,[-1,4,1])*-1.0e9)

reshaped = tf.reshape(x,[tf.shape(x)[0], tf.shape(x)[1]*tf.shape(x)[2]])

with tf.Session() as session:
    session.run(init)
    #print(session.run([logits]))
    print(session.run([adding]))
    #print(posMat)
