import tensorflow as tf
import numpy as np
import __dataset_manager__ as manager
from DAN import Classifier
from scipy.stats import pearsonr as pearson
import math

BATCH_SIZE = 8
DW = 300
NUM_EPOCHS = 5
lr = tf.placeholder(tf.float32, [])

squadtrain_dataset = manager.getFilterDataset('squadParagraphFilter.tfrecord', batchSize = BATCH_SIZE, shuffle=True, repeat=True)
squadvali_dataset = manager.getFilterDataset('squadParagraphFilter_vali.tfrecord', batchSize = BATCH_SIZE, repeat=True)

snlitrain_dataset = manager.getsnliDataset('snliTrain.tfrecord', batchSize=BATCH_SIZE, shuffle=True, repeat=True)
snlivali_dataset = manager.getsnliDataset('snliVali.tfrecord', batchSize=BATCH_SIZE, shuffle=True, repeat=True)

wikitrain_dataset = manager.getWikiDataset('wikiArticle_train.tfrecords', batchSize=BATCH_SIZE, shuffle=True, repeat=True)
wikivali_dataset = manager.getWikiDataset('wikiArticle_vali.tfrecords', batchSize=BATCH_SIZE, shuffle=True, repeat=True)

squadtrain_iterator = squadtrain_dataset.make_one_shot_iterator()
squadtrain_s1, squadtrain_s2, squadtrain_label = squadtrain_iterator.get_next()

squadvali_iterator = squadvali_dataset.make_one_shot_iterator()
squadvali_s1, squadvali_s2, squadvali_label = squadvali_iterator.get_next()

# snlitrain_iterator = snlitrain_dataset.make_one_shot_iterator()
# snlitrain_s1, snlitrain_s2, snlitrain_label = snlitrain_iterator.get_next()

# snlivali_iterator = snlivali_dataset.make_one_shot_iterator()
# snlivali_s1, snlivali_s2, snlivali_label = snlivali_iterator.get_next()

wikitrain_iterator = wikitrain_dataset.make_one_shot_iterator()
wikitrain_s1, wikitrain_s2, wikitrain_label = wikitrain_iterator.get_next()

wikivali_iterator = wikivali_dataset.make_one_shot_iterator()
wikivali_s1, wikivali_s2, wikivali_label = wikivali_iterator.get_next()

model = Classifier()

#forward pass

squadtrain_value = model.call(squadtrain_s1, squadtrain_s2, training=True)
squadvali_value = model.call(squadvali_s1, squadvali_s2, training=False)

# snlitrain_value = model.call(snlitrain_s1, snlitrain_s2, training=True)
# snlivali_value = model.call(snlivali_s1, snlivali_s2, training=False)

wikitrain_value = model.call(wikitrain_s1, wikitrain_s2, training=True)
wikivali_value = model.call(wikivali_s1, wikivali_s2, training=False)

squadtrain_loss = tf.losses.mean_squared_error(squadtrain_label, squadtrain_value) #+ tf.losses.get_regularization_loss()
#snlitrain_loss = tf.losses.softmax_cross_entropy(snlitrain_label, snlitrain_value) #+ tf.losses.get_regularization_loss()
wikitrain_loss = tf.losses.mean_squared_error(wikitrain_label, wikitrain_value) #+ tf.losses.get_regularization_loss()

squadvali_loss = tf.losses.mean_squared_error(squadvali_label, squadvali_value) #+ tf.losses.get_regularization_loss()
# snlivali_loss = tf.losses.softmax_cross_entropy(snlivali_label, snlivali_value) #+ tf.losses.get_regularization_loss()
wikivali_loss = tf.losses.mean_squared_error(wikivali_label, wikivali_value) #+ tf.losses.get_regularization_loss()

train_loss = (squadtrain_loss + wikitrain_loss * 0.1)
vali_loss = (squadvali_loss + wikivali_loss * 0.1)

train_summary = tf.summary.scalar('training_loss', train_loss)
vali_summary = tf.summary.scalar('validation_loss', vali_loss)

optimizer = tf.train.AdamOptimizer(learning_rate=lr)
train_op = optimizer.minimize(train_loss)

trainy = tf.squeeze(squadtrain_label)
trainx = tf.squeeze(squadtrain_value)

train_squadaccuracy = tf.reduce_sum(tf.where(tf.abs(trainy-trainx) < 0.2, tf.ones(tf.shape(trainx)[0]), tf.zeros(tf.shape(trainx)[0])))

trainy1 = tf.squeeze(wikitrain_label)
trainx1 = tf.squeeze(wikitrain_value)

train_wikiaccuracy = tf.reduce_sum(tf.where(tf.abs(trainy1-trainx1) < 0.2, tf.ones(tf.shape(trainx1)[0]), tf.zeros(tf.shape(trainx1)[0])))


train_accSum = tf.summary.scalar('average training_accuracy', (train_squadaccuracy + train_wikiaccuracy) / 2)

valiy = tf.squeeze(squadvali_label)
valix = tf.squeeze(squadvali_value)

vali_squadaccuracy = tf.reduce_sum(tf.where(tf.abs(valiy-valix) < 0.2, tf.ones(tf.shape(valix)[0]), tf.zeros(tf.shape(valix)[0])))

valiy1 = tf.squeeze(wikivali_label)
valix1 = tf.squeeze(wikivali_value)

vali_wikiaccuracy = tf.reduce_sum(tf.where(tf.abs(valiy1-valix1) < 0.2, tf.ones(tf.shape(valix1)[0]), tf.zeros(tf.shape(valix1)[0])))

vali_accSum = tf.summary.scalar('average validation_accuracy', (vali_wikiaccuracy + vali_squadaccuracy) / 2)

init = tf.global_variables_initializer()
init_l = tf.local_variables_initializer()

saver = tf.train.Saver()

iteration = 0
divide = 0
step = 1

checkpoint = int(651595/BATCH_SIZE)

def trainSchedule(step, dim=DW, warmup_step=20000):
    return 1e-4 * 1/(math.log(step+1, 10))
    #return dim ** -0.5 * min(step ** -0.5, step * warmup_step ** -1.5)

# with tf.Session() as sess:
#     saver.restore(sess,'./paragraphFilter_good/')
#     print('Model restored')
#     loss_total = 0.0
#     SICK = 0.0
#     tot_acc = 0.0
#     iteration = 0
#     try:
#       for _ in range(int(50000/BATCH_SIZE)): #int(5700/BATCH_SIZE))
#         losses, X, Y, accr = sess.run([vali_loss, x_vali, y_vali, vali_accuracy])
#         print('X: ',X)
#         print('Y: ',Y)
#         corr = pearson(X,Y)[0]
#         loss_total += losses ** 0.5
#         SICK += corr
#         tot_acc += accr
#         # print("Accuracy: ", sess.run([acc]))
#         # print()
#         iteration += 1
#         print("Validation Average loss: ", loss_total / (iteration), "Average SICK evaluation: ", SICK / (iteration*BATCH_SIZE), " Average Accuracy: ", tot_acc / ((iteration+1) * BATCH_SIZE))
#
#     except tf.errors.OutOfRangeError:
#       pass


with tf.Session() as sess:
    sess.run(init)
    sess.run(init_l)
    #saver.restore(sess,'./paragraphFilter/')
    writer = tf.summary.FileWriter('./graphs', sess.graph)

    for epoch in range(NUM_EPOCHS):
      loss_total = 0.0
      squadtot_acc = 0.0
      wikitot_acc = 0.0
      iteration = 0

      valisquadacc = 0.0
      valiwikiacc = 0.0
      try:
          for _ in range(checkpoint):
            learnRate = trainSchedule(step) #000011798376
            _ , losses, squadaccur, wikiaccur = sess.run([train_op, train_loss, train_squadaccuracy, train_wikiaccuracy], feed_dict={lr:1e-4})

            #print(sess.run([squadtrain_s1]))
            t1, t2 = sess.run([train_summary,train_accSum])
            writer.add_summary(t1, step)
            writer.add_summary(t2, step)
            loss_total += losses ** 0.5
            squadtot_acc += squadaccur
            wikitot_acc += wikiaccur

            squad, wiki = sess.run([vali_squadaccuracy, vali_wikiaccuracy])

            valisquadacc += squad
            valiwikiacc += wiki
            if(iteration % 100 == 0):
                # print(sess.run([squadvali_label]))
                # print(sess.run([squadvali_value]))
                print("Epoch: ",epoch+1, " Iteration ", iteration)
                print(" Average loss: ", loss_total / (iteration + 1), "Average squad accuracy: ", squadtot_acc / ((iteration+1) * BATCH_SIZE), " Average wiki accuracy: ", wikitot_acc / ((iteration+1) * BATCH_SIZE))
                print(valisquadacc / ((iteration+1) * BATCH_SIZE))
                print(valiwikiacc / ((iteration+1) * BATCH_SIZE))
                # print(sess.run([wikitrain_value, wikitrain_label]))
                # print(sess.run([squadtrain_value, squadtrain_label]))
            # print("Accuracy: ", sess.run([acc]))
            # print()

            iteration += 1
            step += 1

            if(iteration%30000 == 0):
                loss_total = 0.0
                SICK = 0.0
                squadtot_acc = 0.0
                wikitot_acc = 0.0
                iteration = 0

                valisquadacc = 0.0
                valiwikiacc = 0.0



      except tf.errors.OutOfRangeError:
          pass
      print('saving transformer model...')
      saver.save(sess, './paragraphFilter_gen1/')
      # similarity, label = sess.run([cosine_similarity, train_label])
      loss_total = 0.0
      squadtot_acc = 0.0
      wikitot_acc = 0.0
      iteration = 0
      try:
          for _ in range(int(80000/BATCH_SIZE)): #int(5700/BATCH_SIZE))
            losses, squadaccur, wikiaccur = sess.run([vali_loss, vali_squadaccuracy, vali_wikiaccuracy])

            loss_total += losses ** 0.5
            squadtot_acc += squadaccur
            wikitot_acc += wikiaccur
            v1, v2 = sess.run([vali_summary,vali_accSum])
            writer.add_summary(v1, step)
            writer.add_summary(v2, step)
            # print("Accuracy: ", sess.run([acc]))
            # print()
            iteration += 1

      except tf.errors.OutOfRangeError:
          pass

      print(" Average loss: ", loss_total / (iteration + 1), "Average squad accuracy: ", squadtot_acc / ((iteration+1) * BATCH_SIZE), " Average wiki accuracy: ", wikitot_acc / ((iteration+1) * BATCH_SIZE))

      # print('similarity: ', similarity)
      # print('label: ', label)
