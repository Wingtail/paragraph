import numpy as np
import tensorflow as tf
import nltk
from tqdm import tqdm
import pickle
import csv
import json
import random
from sifEmbed import SIF
from scipy.stats import norm
import chars2vec

class Model():
    def __init__(self):
        self.charModel = chars2vec.load_model('eng_300')

BUFFER = 204800
ID = 2
DW = 300 #word vector dimension
PADDING_MAX = 300

# './datasets/glove.6B/glove.6B.300d.txt'

###Variables for Evaluation###

vocabIndex = {}
wordTuple = []

#####

vocabDict = {'<pad>': 0,
            '[VEC]': 1
}



def loadVocabularyFile(directory):
    #Assuming GloVe format
    global ID

    lookuptable = [np.zeros(300), np.random.rand(300)]

    with open(directory, 'r') as f:
        lines = f.readlines(BUFFER)
        with tqdm(total=(1e5 * 4)) as pbar:
            while(lines):
                for line in lines:
                    line = line.rstrip()
                    splitPoint = line.index(' ')

                    wordVec = np.fromstring(line[splitPoint+1:], dtype='float32', sep=' ')
                    word = line[:splitPoint]

                    vocabDict[word] = ID
                    lookuptable.append(wordVec)

                    ID += 1
                    pbar.update(1)
                lines = f.readlines(BUFFER)

    lookup = np.zeros((len(lookuptable), 300))
    for i in range(len(lookuptable)):
        lookup[i] = lookuptable[i]

    pickle.dump(lookup, open('lookuptable.pckl', 'wb'))

def checkVocabExist(word):
    global ID
    if(word not in vocabDict):
        vocabDict[word] = (np.random.uniform(low=-1.0, high=1.0, size=DW), ID)

        ##Eval
        vocabIndex[ID] = word
        ##

        ID += 1

def sentence_to_tokenID(sentence):
    sentence = sentence.lower()
    tokens = ['[VEC]']
    tokens.extend(nltk.word_tokenize(sentence))

    ##Evaluation
    wordTuple.append(tokens)
    ####

    tokenID = np.zeros(PADDING_MAX, dtype='int')

    index = 0
    for token in tokens:
        checkVocabExist(token)
        tokenID[index] = vocabDict[token][1]
        index += 1

    return tokenID

#Assume value is a numpy 1-D array
def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=value))

def _float_feature(value):
    return tf.train.Feature(float_list=tf.train.FloatList(value=value))


#Starting from here, function is task specific

#Assuming it is sentence pair
#Assuming label is list of floats
def writeTfrecord(s1, s2, label, writer):
    tok1 = sentence_to_tokenID(s1)
    tok2 = sentence_to_tokenID(s2)

    tok1Feature = _int64_feature(tok1)
    tok2Feature = _int64_feature(tok2)
    labelFeature = _float_feature(label)

    featureDict = {'s1': tok1Feature, 's2': tok2Feature, 'label':labelFeature}
    features = tf.train.Features(feature=featureDict)
    example = tf.train.Example(features=features)


    writer.write(example.SerializeToString())

def writeSquadParagraph(s1, s2, label, writer):
    tok1Feature = _float_feature(s1)
    tok2Feature = _float_feature(s2)
    labelFeature = _float_feature(label)

    featureDict = {'p1': tok1Feature, 'p2': tok2Feature, 'label':labelFeature}
    features = tf.train.Features(feature=featureDict)
    example = tf.train.Example(features=features)

    writer.write(example.SerializeToString())

def writeWikiParagraph(s1, s2, label, writer):
    tok1Feature = _int64_feature(s1)
    tok2Feature = _int64_feature(s2)
    labelFeature = _float_feature(label)

    featureDict = {'p1': tok1Feature, 'p2': tok2Feature, 'label':labelFeature}
    features = tf.train.Features(feature=featureDict)
    example = tf.train.Example(features=features)

    writer.write(example.SerializeToString())

def writeLookupTable(directory):
    keyList = list(vocabDict.keys())
    lookuptable = np.zeros((len(keyList), DW), dtype='float32')
    index = 0
    for key in keyList:
        lookuptable[index] = vocabDict[key][0]
        index += 1

    pickle.dump(lookuptable, open(directory, 'wb'))
    return lookuptable

def writeVocabulary(directory):
    pickle.dump(vocabDict, open(directory, 'wb'))

def loadSTS(directory, outDirectory):
    sample = 0
    #Assuming it is SICK-R scoring

    with tf.io.TFRecordWriter(outDirectory) as writer:
        with open(directory,'r') as f:
            lines = f.readlines(204800)
            while(lines):
                for line in lines:
                    extractedData = line.strip('\n').split('\t')
                    score = float(extractedData[4]) / 5.0 # normalization

                    s1 = extractedData[5]
                    s2 = extractedData[6]

                    writeTfrecord(s1, s2, [score], writer)

                lines = f.readlines(204800)

def nli_to_label(nliTag):
    label = np.zeros(3)
    if(nliTag == "entailment"):
        label[0] = 1.0
    elif(nliTag == "neutral"):
        label[1] = 1.0
    else:
        label[2] = 1.0

    return label

def loadSNLI(directory, outDirectory):
    sentences = []
    labels = []
    counter = 0
    vocabDict = pickle.load(open('vocabDict.pckl', "rb"))
    vecLookup = pickle.load(open('lookuptable.pckl', "rb"))

    model = Model()

    unkWords = []

    with tf.io.TFRecordWriter(outDirectory) as writer:
        with open(directory, 'r') as f:
            with tqdm(total=570000) as pbar:
                lines = f.readlines(204800)
                while lines:
                    for line in lines:
                        y = json.loads(line)
                        label = nli_to_label(y['gold_label'])
                        check = list(y.keys())
                        if('sentence1' not in check and 'sentence2' not in check and 'gold_label' not in check):
                            print('ERROR!')
                            break

                        s1 = nltk.word_tokenize(y['sentence1'].lower())
                        s1Index = np.zeros(PADDING_MAX, dtype=int)

                        s2 = nltk.word_tokenize(y['sentence2'].lower())
                        s2Index = np.zeros(PADDING_MAX, dtype=int)

                        for i in range(len(s1)):
                            if(s1[i] in vocabDict):
                                # print(vocabDict[s1[i]])
                                s1Index[i] = vocabDict[s1[i]]
                            # else:
                            #     ID = len(list(vocabDict.keys()))
                            #     vocabDict[s1[i]] = ID
                            #     s1Index[i] = ID
                            #     unkWords.append(s1[i])

                        for i in range(len(s2)):
                            if(s2[i] in vocabDict):
                                s2Index[i] = vocabDict[s2[i]]
                            # else:
                            #     ID = len(list(vocabDict.keys()))
                            #     vocabDict[s2[i]] = ID
                            #     s2Index[i] = ID
                            #     unkWords.append(s2[i])

                        writeWikiParagraph(s1Index, s2Index, label, writer)
                        pbar.update(1)

                    lines = f.readlines(204800)
    # print('converting unkwords in char vectors')
    # vecLookup = np.append(vecLookup, model.charModel.vectorize_words(unkWords))
    # print('dumping lookuptable')
    # pickle.dump(vecLookup, open('lookuptable.pckl', 'wb'))
    # print('complete')

#Driver


# loadVocabularyFile('./datasets/glove.6B/glove.6B.300d.txt')
# vocabDict = pickle.load(open('vocabDict.pckl', 'rb'))
# loadSNLI('datasets/snli_1.0/snli_1.0_dev.jsonl', 'snliVali.tfrecord')
# lookuptable = writeLookupTable('lookuptable.pckl')
# writeVocabulary('vocabDict.pckl')
#
# print('wrote test data')
# print('# of words: ', ID)

#
#
# loadSTS('./datasets/stsbenchmark/sts-train.csv', 'trainSTS.tfrecord')
# print('wrote train data')
# loadSTS('./datasets/stsbenchmark/sts-dev.csv', 'valiSTS.tfrecord')
# print('wrote dev data')
# loadSTS('./datasets/stsbenchmark/sts-test.csv', 'testSTS.tfrecord')

#

# pickle.dump(vocabIndex, open('vocabIndex.pckl', 'wb'))
# pickle.dump(wordTuple, open('wordTuple.pckl', 'wb'))
#

def loadQQP(directory):
    with open(directory, "r") as f:
        lines = f.readlines(204800)
        while(lines):
            for line in lines:
                line = line.rstrip()
                line = line.replace("\"", '')
                line = line.replace("\'", '')
                data = line.split(',')

                data[3]
                data[4]

                print(data)
            f.readlines(204800)

def loadSquad(directory, outDirectory):
    topics = []
    with open(directory, 'r') as f:
        squad = json.load(f)
        data = squad['data']
        for topic in data:
            ps = []
            paragraphs = topic['paragraphs']
            for paragraph in paragraphs:
                p = paragraph['context']
                q = []
                for question in paragraph['qas']:
                    q.append(question['question'])
                ps.append((p,q))
            topics.append(ps)

    pickle.dump(topics, open(outDirectory, 'wb'))

    return topics
    # squad = json.loads()

def generateWikiData(paragraphs, summarySentences, overview, sif, writer):
    paragraphList = [] #Expecting around 30,000
    paragraphList.extend(list(map(lambda paragraph:nltk.sent_tokenize(paragraph), paragraphs)))
    paragraphList.extend(list(map(lambda sentence:[sentence], summarySentences)))
    paragraphList.extend(list(map(lambda over:nltk.sent_tokenize(over), overview)))


    print('embedding paragraphs')
    paragraphList = sif.sifEmbedParagraphs(paragraphList)
    print('done')

    batchLength = len(paragraphs) #paragraphs, sentences, overview lengths are equal

    with tqdm(total=batchLength) as pbar:
        pbar.set_description('paragraph list re-initializing')
        for i in range(batchLength):
            paragraphs[i] = paragraphList[i]
            pbar.update(1)


    with tqdm(total=batchLength) as pbar:
        pbar.set_description('sentence list re-initializing')
        for i in range(batchLength):
            summarySentences[i] = paragraphList[i+batchLength]
            pbar.update(1)

    with tqdm(total=batchLength) as pbar:
        pbar.set_description('overview re-initializing')
        for i in range(batchLength):
            overview[i] = paragraphList[i+(2*batchLength)]
            pbar.update(1)

    paragraphList = []

    with tqdm(total=batchLength) as pbar:
        pbar.set_description('writing to tfrecords')
        for i in range(batchLength):
            writeWikiParagraph(paragraphs[i], summarySentences[i], [1.0], writer)
            writeWikiParagraph(paragraphs[i], overview[i], [0.5], writer)
            pbar.update(1)


def loadWiki():
    paragraphs = []
    summarySentences = []
    overview = []

    vocabDict = pickle.load(open('sif_vocabDict.pckl', "rb"))
    vecLookup = pickle.load(open('sif_vecLookup.pckl', "rb"))
    freqLookup = pickle.load(open('sif_freqLookup.pckl', "rb"))

    print('loaded')
    params = {
    'alpha': 1e-3,
    'dim':300,
    'vocabulary': vocabDict,
    'vecLookup':vecLookup,
    'freqLookup':freqLookup
    }

    sif = SIF(params)

    train_writer = tf.io.TFRecordWriter('wikiArticle_train.tfrecords')
    vali_writer = tf.io.TFRecordWriter('wikiArticle_vali.tfrecords')

    counter = 0

    with tqdm(total=520000) as pbar:
        with open("wikihowSep.csv", "r") as f:
            reader = csv.reader(f, delimiter=',')
            for line in reader:
                paragraphs.append(line[2])
                summarySentences.append(line[1])
                overview.append(line[0])

                if(len(paragraphs) == 2000 and counter < 500000):
                    generateWikiData(paragraphs, summarySentences, overview, sif, train_writer)
                    paragraphs = []
                    summarySentences = []
                    overview = []
                elif(len(paragraphs) == 2000 and counter >= 500000):
                    generateWikiData(paragraphs, summarySentences, overview, sif, vali_writer)
                    paragraphs = []
                    summarySentences = []
                    overview = []

                if(counter == 500000):
                    train_writer.close()
                elif(counter == 520000):
                    vali_writer.close()
                    break

                counter += 1
                pbar.update(1)


    # group = (paragraphs, summarySentences)
    #
    # pickle.dump(group, open('wikiTopic-train.pckl', 'wb'))


def randomExcept(n, end, start = 1):
    if n is None:
        return range(start, end)
    else:
        return list(range(start, n)) + list(range(n+1, end))

def prepareSquad(saveDirectory, directory, outDirectory, loadDirectory = None):
    if(loadDirectory is None):
        topics = loadSquad(directory, outDirectory)
        paragraphs = []
        vocabDict = pickle.load(open('sif_vocabDict.pckl', "rb"))
        vecLookup = pickle.load(open('sif_vecLookup.pckl', "rb"))
        freqLookup = pickle.load(open('sif_freqLookup.pckl', "rb"))

        print('loaded')
        params = {
        'alpha': 1e-3,
        'dim':300,
        'vocabulary': vocabDict,
        'vecLookup':vecLookup,
        'freqLookup':freqLookup
        }

        sif = SIF(params)

        for topic in topics:
            #print(topic[0])
            for paragraph in topic:
                paragraphs.append(nltk.sent_tokenize(paragraph[0]))
                questions = [[question] for question in paragraph[1]]
                paragraphs.extend(questions)

        partition = []
        chunk = 40000
        if(len(paragraphs) > chunk):
            for i in range(0, len(paragraphs), chunk):
                partition.append(paragraphs[i:i+chunk])
        else:
            partition = [paragraphs]

        paragraphVectors = []

        nPartitions = len(partition)
        chunkN = 1
        for chunk in partition:
            print(chunkN, '/', nPartitions, 'chunks')
            paragraphVectors.extend(sif.sifEmbedParagraphs(chunk))
            chunkN += 1
            print(len(paragraphVectors))
            print(paragraphVectors[0])

        # print(paragraphVectors)
        # print(paragraphVectors[0])

        pickle.dump(paragraphVectors, open('squadParagraphs_vali.pckl', 'wb'))

        index = 0

        # paragraphs = []


        for i in range(len(topics)):
            for j in range(len(topics[i])):
                # paragraph = []
                # questions = []
                #paragraph.append(paragraphVectors[index])
                tup = []

                tup.append(paragraphVectors[index])
                index += 1

                questions = []
                for k in range(len(topics[i][j][1])):
                    # questions.append(paragraphVectors[index])
                    questions.append(paragraphVectors[index])
                    index += 1
                tup.append(questions)
                topics[i][j] = tuple(tup)
                # paragraph.append(questions)
                # paragraphs.append(paragraph)


        print(len(paragraphs))
        pickle.dump(topics, open('squadTopics_vali.pckl', 'wb'))
    else:
        topics = pickle.load(open(loadDirectory, 'rb'))

    #topics = pickle.load(open('squadTopics.pckl', 'rb'))

    labelSmoothing = 0.2

    print('loaded')
    with tf.io.TFRecordWriter(saveDirectory) as writer:
        with tqdm(total=651595) as pbar:
            for i in range(len(topics)):
                for j in range(len(topics[i])):
                    for k in range(len(topics[i][j][1])):
                        #paragraph = np.zeros(300, dtype=float)
                        paragraph = topics[i][j][0]
                        question = topics[i][j][1][k]
                        noise = np.abs(np.random.normal(0.0,0.1,1))
                        label = [1.0 - noise]
                        writeSquadParagraph(paragraph, question, label, writer)
                        relatedParagraphs = randomExcept(j, len(topics[i]), 0)
                        unrelatedTopics = randomExcept(None, len(topics), 0)
                        pbar.update(1)
                        for _ in range(1): #related but not having question
                            choice = random.choice(relatedParagraphs)
                            paragraph = topics[i][choice][0]
                            noise = np.abs(np.random.normal(0.0,0.1,1))
                            label = [0.1 + noise]
                            writeSquadParagraph(paragraph, question, label, writer)
                            pbar.update(1)
                        for _ in range(1):
                            topic_choice = random.choice(unrelatedTopics)
                            unrelatedParagraphs = randomExcept(None, len(topics[topic_choice]), 0)
                            paragraph_choice = random.choice(unrelatedParagraphs)
                            paragraph = topics[topic_choice][paragraph_choice][0]
                            noise = np.abs(np.random.normal(0.0,0.1,1))
                            label = [0.0 + noise]
                            writeSquadParagraph(paragraph, question, label, writer)
                            pbar.update(1)

    print('complete')

#
loadVocabularyFile('./datasets/glove.6B/glove.6B.300d.txt')
pickle.dump(vocabDict, open('vocabDict.pckl', 'wb'))
loadSNLI('./datasets/snli_1.0/snli_1.0_train.jsonl', 'snliTrain.tfrecord')
loadSNLI('./datasets/snli_1.0/snli_1.0_dev.jsonl', 'snliVali.tfrecord')
# loadWiki()
# prepareSquad('squadParagraphFilter.tfrecord', "./datasets/train-v2.0.json", "squad-trainTopic.pckl")
# prepareSquad('squadParagraphFilter_vali.tfrecord', "./datasets/dev-v2.0.json", "squad-valiTopic.pckl")
# prepareSquad('squadParagraphFilter_vali.tfrecord')

# loadQQP("./datasets/questions.csv")



def checkSentenceConvertible():
    loadVocabularyFile('./datasets/glove.6B/glove.6B.300d.txt')
    lookuptable = writeLookupTable('lookuptable.pckl')
    sentenceIndex = sentence_to_tokenID('The cat flew on top of me.')

    print(sentenceIndex)

    for index in sentenceIndex:
        print(vocabIndex[index])

        fromVocabDictArr = vocabDict[vocabIndex[index]][0]
        fromLookupArr = lookuptable[index]

        if(np.array_equal(fromVocabDictArr, fromLookupArr) == True):
            print('Correct')
        else:
            print('Incorrect')
