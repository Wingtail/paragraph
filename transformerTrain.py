import tensorflow as tf
import numpy as np
import pickle
import __dataset_manager__ as manager
import transformer
from DAN import DAN
from transformer import Transformer



DW = 300
N_HEADS = 4
INPUT_SIZE = 300
BATCH_SIZE = 32
NUM_EPOCHS = 3

lr = tf.placeholder(tf.float32, [])
lookuptable = pickle.load(open('lookuptable.pckl', 'rb'))

wordSize = len(lookuptable)

vocabDict = tf.placeholder(tf.float32, [wordSize, 300])

train_dataset = manager.getsnliDataset('snliTrain.tfrecord', batchSize = BATCH_SIZE, shuffle=True, repeat=True)
vali_dataset = manager.getsnliDataset('snliVali.tfrecord', batchSize = BATCH_SIZE, repeat=True)

train_iterator = train_dataset.make_one_shot_iterator()
train_s1, train_s2, train_label = train_iterator.get_next()

vali_iterator = vali_dataset.make_one_shot_iterator()
vali_s1, vali_s2, vali_label = vali_iterator.get_next()



vectorModel = Transformer(DW, N_HEADS, INPUT_SIZE, BATCH_SIZE, 3)

#forward pass

train_vec1 = vectorModel.call(train_s1, vocabDict)
train_vec2 = vectorModel.call(train_s2, vocabDict)

nliLayers = []

for _ in range(3):
    nliLayers.append(tf.layers.Dense(512, use_bias=True, activation=tf.nn.elu))

nliLayers.append(tf.layers.Dense(3, use_bias=True, activation=tf.nn.softmax))

nliLogits = transformer.nli(train_vec1, train_vec2, nliLayers)

nli_loss = transformer.crossEntropy(nliLogits, train_label)
nli_acc = transformer.nliAcc(nliLogits, train_label)

# train_loss = transformer.mseLoss(cosine_similarity, train_label) #+ crossEntropy(nliOut, output['entailment']) + crossEntropy(qqpOut, output['semantic'])
# train_acc = transformer.accuracy(cosine_similarity, train_label)

optimizer = tf.train.AdamOptimizer(learning_rate=lr)
train_op = optimizer.minimize(nli_loss)

vali_vec1 = vectorModel.call(vali_s1, vocabDict)
vali_vec2 = vectorModel.call(vali_s2, vocabDict)

valiNli = transformer.nli(vali_vec1, vali_vec2, nliLayers)

vali_loss = transformer.crossEntropy(valiNli, vali_label)
vali_acc = transformer.nliAcc(valiNli, vali_label)

init = tf.global_variables_initializer()
initL = tf.local_variables_initializer()

saver = tf.train.Saver()

iteration = 0
divide = 0
step = 1

checkpoint = 570000/BATCH_SIZE

def trainSchedule(step, dim=DW, warmup_step=1920):
    return dim ** -0.5 * min(step ** -0.5, step * warmup_step ** -1.5)


with tf.Session() as sess:
    sess.run(init)
    sess.run(initL)

    for epoch in range(NUM_EPOCHS):
      loss_total = 0.0
      acc_total = 0.0
      iteration = 0
      try:
          for _ in range(int(checkpoint)):
            learnRate = trainSchedule(step)
            # print(sess.run(train_s1).shape)
            _ , losses, accuracy = sess.run([train_op, nli_loss, nli_acc], feed_dict={vocabDict:lookuptable, lr:1e-4})
            loss_total += losses
            acc_total += (accuracy * 100.0)
            print("Epoch: ",epoch, " Iteration ", iteration)
            print(" Average loss: ", loss_total / (iteration + 1), "Accuracy: ", acc_total / ((iteration+1) * BATCH_SIZE), "%")
            # print("Accuracy: ", sess.run([acc]))
            # print()

            iteration += 1
            step += 1

            if(iteration%1000 == 0):
                loss_total = 0.0
                acc_total = 0.0
                iteration = 0


      except tf.errors.OutOfRangeError:
          pass
      print('saving transformer model...')
      # similarity, label = sess.run([cosine_similarity, train_label])
      loss_total = 0.0
      acc_total = 0.0
      iteration = 0
      try:
          for _ in range(int(10000/BATCH_SIZE)): #int(5700/BATCH_SIZE))
            losses, accuracy = sess.run([vali_loss, vali_acc], feed_dict={vocabDict:lookuptable})
            loss_total += losses ** 0.5
            acc_total += accuracy
            # print("Accuracy: ", sess.run([acc]))
            # print()
            iteration += 1

      except tf.errors.OutOfRangeError:
          pass

      print("Validation Average loss: ", loss_total / (iteration), "Average Accuracy: ", acc_total / (iteration*BATCH_SIZE))
      # print('similarity: ', similarity)
      # print('label: ', label)
      saver.save(sess, './transformerCheck/')

      # for inputs, outputs in dataset:
      #   sess.run(train_op, feed_dict={sst1:inputs['sst1'], sst2:inputs['sst2'],
      #   nli1:inputs['nli1'], nli2:inputs['nli2'], qqp1:inputs['qqp1'], qqp2:inputs['qqp2'],
      #   similarity:outputs['similarity'], entailment:outputs['entailment'], semantic:outputs['semantic'],
      #   })
      #
      #   loss_total = loss_total + tot_loss
      #
      #   loss_tot = sess.run(loss_total)

    #   print('Epoch {} - Average MSE: {:.4f}'.format(epoch + 1, loss_tot / (epoch+1)))
    # time_taken = time.time() - time_start
    #
    # print('\nTotal time taken for training (secoonds): {:.2f}'.format(time_taken))
