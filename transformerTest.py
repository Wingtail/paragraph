import transformer
import tensorflow as tf
import pickle
import __dataset_manager__
import numpy as np

DW = 300
N_HEADS = 6
INPUT_SIZE = 300
BATCH_SIZE = 10

multihead = transformer.Multihead(DW, N_HEADS, INPUT_SIZE, BATCH_SIZE)

lookuptable = pickle.load(open("lookuptable.pckl", "rb"))

dataset = __dataset_manager__.getDataset('trainSTS.tfrecord', batchSize = BATCH_SIZE)
dataset = dataset.take(1)

embed = transformer.Embedding(lookuptable, DW)

iterator = dataset.make_one_shot_iterator()
s1, s2, label = iterator.get_next()

sentence1 = embed(s1)
sentence2 = embed(s2)

a = multihead(sentence1)
b = multihead(sentence2)

init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    val1, val2 = sess.run([a,b])
    print('val1: ', val1)
    print('val2: ', val2)
