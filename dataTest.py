import tensorflow as tf
import pickle
import numpy as np

DW = 300

def parse(record):
    featuresDict = {'s1': tf.FixedLenFeature([DW], tf.int64),
                    's2': tf.FixedLenFeature([DW], tf.int64),
                    'label': tf.FixedLenFeature([1], tf.float32)
    }

    parsedExample = tf.parse_single_example(record, featuresDict)
    return (parsedExample['s1'], parsedExample['s2'], parsedExample['label'])

dataset = tf.data.TFRecordDataset('trainSTS.tfrecord')
dataset = dataset.map(parse)

training_iterator = dataset.make_one_shot_iterator()

s1, s2, label = training_iterator.get_next()

iteration = 0

vocabDict = pickle.load(open('vocabDict.pckl', 'rb'))
vocabIndex = pickle.load(open('vocabIndex.pckl', 'rb'))
wordTuple = pickle.load(open('wordTuple.pckl', 'rb'))
lookuptable = pickle.load(open('lookuptable.pckl', 'rb'))

print('lookuptable size: ', lookuptable.shape)

def index_to_word(arr):

    sentence = []
    try:
        for index in arr:
            sentence.append(vocabIndex[index])
    except:
        pass

    return sentence

class Embedding(tf.layers.Layer):
    def __init__(self, wordEmbed, dW):
        super(Embedding,self).__init__()
        self.params = wordEmbed
        self.dW = dW

    def call(self, x):
        return tf.nn.embedding_lookup(self.params, x)

def index_to_vec(indices):
    matrix = np.zeros((300,300))

    count = 0
    for index in indices:
        #print(vocabDict[vocabIndex[index]][0])
        matrix[count] = vocabDict[vocabIndex[index]][0]
        count += 1
    return matrix




valiChecks = []

with tf.Session() as sess:
    try:
        while(True):
            sent1, sent2 = sess.run([s1, s2])
            s1Mat = index_to_vec(sent1)
            s2Mat = index_to_vec(sent2)
            valiChecks.append((s1Mat, s2Mat))
    except:
        pass

print('made vali data')

dataset = tf.data.TFRecordDataset('trainSTS.tfrecord')
dataset = dataset.map(parse)
dataset = dataset.batch(32)


training_iterator = dataset.make_one_shot_iterator()

s1, s2, label = training_iterator.get_next()

index = 0
embedding = Embedding(lookuptable, 300)
a = embedding(s1)
b = embedding(s2)

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    # try:
    while True:
        sent1, sent2, l = sess.run([a,b,label])
        print(iteration, 'th turn')

        print(sent1)

        # if(np.allclose(sent1, valiChecks[iteration][0]) and np.allclose(sent2, valiChecks[iteration][1])):
        #     pass
        # else:
        #     print('ERROR!')
        #     print(sent1)
        #     print(valiChecks[iteration][0])
        #     break

        index += 1

        iteration += 1

    # except:
    #     pass
