import tensorflow as tf
import numpy as np

DW = 300

def parse(record):
    featuresDict = {'p1': tf.FixedLenFeature([DW], tf.int64),
                    'p2': tf.FixedLenFeature([DW], tf.int64),
                    'label': tf.FixedLenFeature([3], tf.float32)
    }

    parsedExample = tf.parse_single_example(record, featuresDict)
    return (parsedExample['p1'], parsedExample['p2'], parsedExample['label'])

def filter_parse(record):
    featuresDict = {'p': tf.FixedLenFeature([DW], tf.float32),
                    'q': tf.FixedLenFeature([DW], tf.float32),
                    'label': tf.FixedLenFeature([1], tf.float32)
    }

    parsedExample = tf.parse_single_example(record, featuresDict)
    return (parsedExample['p'], parsedExample['q'], parsedExample['label'])

def wiki_parse(record):
    featuresDict = {'p1': tf.FixedLenFeature([DW], tf.float32),
                    'p2': tf.FixedLenFeature([DW], tf.float32),
                    'label': tf.FixedLenFeature([1], tf.float32)
    }

    parsedExample = tf.parse_single_example(record, featuresDict)
    return (parsedExample['p1'], parsedExample['p2'], parsedExample['label'])

def getsnliDataset(directory, batchSize=1, shuffle=False, repeat=False):
    dataset = tf.data.TFRecordDataset(directory)
    dataset = dataset.map(parse)
    if(shuffle == True):
        dataset = dataset.shuffle(int(DW*0.4) + 3 * batchSize)
    dataset = dataset.repeat()
    dataset = dataset.batch(batchSize)
    dataset = dataset.prefetch(buffer_size=batchSize*10)
    return dataset

def getFilterDataset(directory, batchSize=1, shuffle=False, repeat=False):
    dataset = tf.data.TFRecordDataset(directory)
    dataset = dataset.map(wiki_parse)
    dataset = dataset.shuffle(100000)
    dataset = dataset.repeat()
    dataset = dataset.batch(batchSize)
    dataset = dataset.prefetch(buffer_size=batchSize*10)

    return dataset

def getWikiDataset(directory, batchSize=1, shuffle=False, repeat=False):
    dataset = tf.data.TFRecordDataset(directory)
    dataset = dataset.map(wiki_parse)
    dataset = dataset.shuffle(int(DW*0.4) + 3 * batchSize)
    dataset = dataset.prefetch(buffer_size=batchSize*10)
    dataset = dataset.batch(batchSize)
    if(repeat == True):
        dataset = dataset.repeat()
    return dataset
