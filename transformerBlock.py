from __future__ import print_function
from __future__ import division
from __future__ import absolute_import
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import numpy as np
from datetime import datetime
from packaging import version
from tensorflow.python.keras.callbacks import TensorBoard
import math
from tensorflow.keras.layers import Embedding


# class SelfAttention(tf.keras.layers.Layer):
#     def __init__(self, dk, **kwargs):
#         self.dk = dk
#         super(SelfAttention, self).__init__(**kwargs)
#
#     def build(self, inputShape):
#         shape = tf.TensorShape((inputShape[2], self.dk)) #considering input as batch dimension
        # self.Q = self.add_weight(name='Q', shape=shape, initializer='uniform', trainable=True)
        # self.K = self.add_weight(name='K', shape=shape, initializer='uniform', trainable=True)
        # self.V = self.add_weight(name='V', shape=shape, initializer='uniform', trainable=True)
#         super(SelfAttention, self).build(inputShape)
#
#
#     def call(self, inputs, mask=None):
#         q = tf.matmul(inputs, self.Q)
#         k = tf.matmul(inputs, self.K)
#         v = tf.matmul(inputs, self.V)
#         kT = tf.transpose(k, perm=[0,2,1])
#         attention = tf.divide(tf.matmul(q,kT), tf.math.sqrt(float(self.dk)))
#
#         #shape = attention.get_shape().as_list()
#         #shape[0] = -1
#         selfAttentionMask = mask * -1e9
#
#         attention = tf.add(attention, selfAttentionMask)
#         attention = tf.nn.softmax(attention)
#         self_attention = tf.matmul(attention, v)
#         return self_attention
#
#     def compute_output_shape(self, inputShape):
#         shape = tf.TensorShape(inputShape).as_list()
#         shape[-1] = self.dk
#         return tf.TensorShape(shape)

class Feedforward(tf.keras.layers.Layer):
    def __init__(self, outDim, **kwargs):
        super(Feedforward, self).__init__(**kwargs)
        self.outDim = outDim

    def build(self, inputShape):
        # shape = tf.TensorShape((inputShape[2], self.outDim))
        self.dense1 = tf.keras.layers.Dense(self.outDim, activation=tf.nn.relu, name='Dense1')
        self.dense2 = tf.keras.layers.Dense(self.outDim, name='Dense2')
        # self.weight1 = self.add_weight(name='W1', shape=shape, initializer='random_normal', trainable=True)
        # self.weight2 = self.add_weight(name='W2', shape=shape, initializer='random_normal', trainable=True)
        # shape = tf.TensorShape((inputShape[1], self.outDim))
        # self.bias1 = self.add_weight(name='bias1', shape=shape, initializer='zeros', trainable=True)
        # self.bias2 = self.add_weight(name='bias2', shape=shape, initializer='zeros', trainable=True)
        super(Feedforward, self).build(inputShape)

    def call(self, inputs, mask=None):
        linear1 = self.dense1(inputs)
        #linear1 = tf.multiply(linear1, mask) #mask is posMask: 0 --> padding, 1 --> not padding
        linear2 = self.dense2(linear1)
        #linear2 = tf.multiply(linear2, mask)
        return linear2

class Multihead(tf.keras.layers.Layer):
    def __init__(self, h, outDim, inputLength, **kwargs):
        super(Multihead, self).__init__(**kwargs)
        self.outDim = outDim
        self.inputLength = inputLength
        self.h = h #number of heads

    def build(self, inputShape):
        #shape = tf.TensorShape((self.outDim, self.outDim)) #assume first dimension is batch
        self.Q = tf.keras.layers.Dense(self.outDim, use_bias=False, name='q')
        self.K = tf.keras.layers.Dense(self.outDim,use_bias=False, name='k')
        self.V = tf.keras.layers.Dense(self.outDim,use_bias=False, name='v')
        self.linear = tf.keras.layers.Dense(self.outDim, use_bias=False, name='concat_dense')
        # self.selfAttentions = []
        # self.dk = int(self.outDim / self.h)
        # for i in range(self.h):
        #     self.selfAttentions.append(SelfAttention(self.dk))
        super(Multihead, self).build(inputShape)

    def split_heads(self, x):
        batch_size = tf.shape(x)[0]

        # Calculate depth of last dimension after it has been split.
        depth = int(self.outDim / self.h)

        # Split the last dimension
        x = tf.reshape(x, [batch_size, self.inputLength, self.h, depth])

        # Transpose the result
        return tf.transpose(x, [0, 2, 1, 3])

    def combine_heads(self, x):
        batch_size = tf.shape(x)[0]
        x = tf.transpose(x, [0, 2, 1, 3])  # --> [batch, length, num_heads, depth]
        return tf.reshape(x, [batch_size, self.inputLength, self.outDim])

    def call(self, inputs, mask=None):
        q = self.Q(inputs)
        k = self.K(inputs)
        v = self.V(inputs)

        q = self.split_heads(q)
        k = self.split_heads(k)
        v = self.split_heads(v)

        depth = (self.outDim / self.h)
        q *= depth ** -0.5

        logits = tf.matmul(q, k, transpose_b=True)
        logits += (tf.expand_dims(mask,1) * -1e9)
        weights = tf.nn.softmax(logits)
        attention_output = tf.matmul(weights, v)

        attention_output = self.combine_heads(attention_output)
        encoded = self.linear(attention_output)
        #encoded = tf.multiply(encoded, mask[1])
        return encoded

    def compute_output_shape(self, inputShape, mask=None):
        shape = tf.TensorShape(inputShape).as_list()
        shape[-1] = self.outDim
        return tf.TensorShape(shape)

class TransformerLayer(tf.keras.layers.Layer):
    def __init__(self, h, outDim, inputShape, **kwargs):
        super(TransformerLayer, self).__init__(**kwargs)
        self.h = h
        self.outDim = outDim
        self.multihead = Multihead(h, outDim, inputShape)
        self.feedforward = Feedforward(outDim)
        self.norm1 = tf.keras.layers.LayerNormalization(center=True, axis=2,beta_initializer=tf.keras.initializers.zeros, trainable=True)
        self.norm2 = tf.keras.layers.LayerNormalization(center=True, axis=2,beta_initializer=tf.keras.initializers.zeros, trainable=True)

    def call(self, inputs, mask=None):
        encoded = self.multihead(inputs, mask=mask)
        residual = tf.add(encoded, inputs)
        normed1 = self.norm1(residual)
        transformed = self.feedforward(encoded)
        residual2 = tf.add(transformed, normed1)
        normed2 = self.norm2(residual2)
        return normed2

    def compute_output_shape(self, inputShape, mask=None):
        shape = tf.TensorShape(inputShape).as_list()
        shape[-1] = self.outDim
        return tf.TensorShape(shape)


class Transformer(tf.keras.Model):
    def __init__(self, N, h, outDim, inputLength):
        super(Transformer, self).__init__(name='Transformer')
        self.outDim = outDim
        self.inputLength = inputLength
        self.N = N
        self.transformerLayers = []
        for i in range(N):
            self.transformerLayers.append(TransformerLayer(h, outDim, inputLength))

        # self.tl1 = TransformerLayer(h, outDim, inputLength)
        # self.tl2 = TransformerLayer(h, outDim, inputLength)

        self.wordDimVals = [10000**(2*k/float(outDim)) for k in range(outDim)]
        self.posMat = None
        self.dense = tf.keras.layers.Dense(outDim, activation=tf.nn.tanh) #tanh for nonlinearity
        self.flatten = tf.keras.layers.Flatten()

    def createMask(self, x):
        b = tf.reduce_sum(tf.math.abs(x), axis=2)
        c = tf.cast(tf.math.equal(b,0.0), dtype=tf.float32) #1 -> padding, 0 -> not padding
        mask = tf.expand_dims(c, axis=1)
        return mask

    def posMask(self, x):
        b = tf.reduce_sum(tf.math.abs(x), axis=2)
        c = tf.cast(tf.math.logical_not(tf.math.equal(b,0.0)), dtype=tf.float32) #0 -> padding, 1 -> not padding
        mask = tf.expand_dims(c, axis=-1)

        #mask = tf.tile(c, [1,1,x.get_shape().as_list()[2]])
        #print("maskShape: ", mask.shape)
        #mask = tf.cast(tf.math.equal(x,0.0), tf.float32)
        return mask

    def createPosMat(self, x):
        self.posMat = np.zeros((self.inputLength, self.outDim), dtype=np.float32)
        for i in range(self.inputLength):
            for j in range(self.outDim):
                if(j%2==0):
                    self.posMat[i][j] = math.sin(i/float(self.wordDimVals[j]))
                else:
                    self.posMat[i][j] = math.cos(i/float(self.wordDimVals[j]))

    def call(self, inputs, training=False):
        x = inputs
        mask = self.createMask(x)
        if(self.posMat is None):
            self.createPosMat(x)
        posMask = self.posMask(x)
        # print('shape: ',self.posMat)
        posMat = tf.multiply(posMask,self.posMat)
        x = tf.add(x, self.posMat)
        # print(x.shape)
        for i in range(self.N):
            x = self.transformerLayers[i](x, mask=mask)

        # x = self.tl1(x, mask=mask)
        # x = self.tl2(x, mask=mask)

        x += (tf.reshape(mask, [-1,self.inputLength,1]) * -1.0e9) #Prevent padding to be max
        x = tf.reduce_max(x, axis=1)
        #x = self.flatten(x)
        #print(training)
        vector = self.dense(x)
        return vector

class cosSim(tf.keras.Model):
    def __init__(self):
        super(cosSim, self).__init__(name='similarity')
    def call(self, vector1, vector2):
        dotted = tf.math.multiply(vector1, vector2)
        dotted = tf.reduce_sum(dotted, axis=1, keepdims=True)
        sum1 = tf.math.sqrt(tf.reduce_sum(tf.math.square(vector1), axis=1, keepdims=True))
        sum2 = tf.math.sqrt(tf.reduce_sum(tf.math.square(vector2), axis=1, keepdims=True))
        denom = tf.math.multiply(sum1, sum2)
        cosineSim = tf.math.divide(dotted, denom)
        return cosineSim

class manhattanSim(tf.keras.Model):
    def __init__(self):
        super(manhattanSim, self).__init__(name='similarity')
    def call(self, vector1, vector2):
        l1 = vector1 - vector2
        l1 = tf.reduce_sum(tf.math.abs(l1), axis=1, keepdims=True)
        manhattan = tf.math.exp(-l1)
        return manhattan

def MultiTask(transformerLayer, maximum, inputDim, embeddingWeights, inputLength):
    sst1 = tf.keras.Input(shape=(inputDim), name='sst1')
    sst2 = tf.keras.Input(shape=(inputDim), name='sst2')

    nli1 = tf.keras.Input(shape=(inputDim), name='nli1')
    nli2 = tf.keras.Input(shape=(inputDim), name='nli2')

    qqp1 = tf.keras.Input(shape=(inputDim), name='qqp1')
    qqp2 = tf.keras.Input(shape=(inputDim), name='qqp2')

    entailDense = tf.keras.layers.Dense(3, activation=tf.nn.softmax, name='entailment')
    semanticEquivalence = tf.keras.layers.Dense(2, activation=tf.nn.softmax, name='semantic')
    #cossim = cosSim()
    manhatSim = manhattanSim()

    embedding = Embedding(len(embeddingWeights), len(embeddingWeights[0]), weights=[embeddingWeights], input_length=inputLength, trainable=False)

    conv_sst1 = embedding(sst1)
    conv_sst2 = embedding(sst2)

    conv_nli1 = embedding(nli1)
    conv_nli2 = embedding(nli2)

    conv_qqp1 = embedding(qqp1)
    conv_qqp2 = embedding(qqp2)

    vec_sst1 = transformerLayer(conv_sst1)
    vec_sst2 = transformerLayer(conv_sst2)

    vec_nli1 = transformerLayer(conv_nli1)
    vec_nli2 = transformerLayer(conv_nli2)

    vec_qqp1 = transformerLayer(conv_qqp1)
    vec_qqp2 = transformerLayer(conv_qqp2)


    mul_nli = tf.math.multiply(vec_nli1, vec_nli2)
    diff_nli = tf.math.abs(tf.math.subtract(vec_nli1, vec_nli2))
    catted_nli = tf.concat([vec_nli1, vec_nli2, diff_nli, mul_nli], 1)

    mul_qqp = tf.math.multiply(vec_qqp1, vec_qqp2)
    diff_qqp = tf.math.abs(tf.math.subtract(vec_qqp1, vec_qqp2))
    catted_qqp = tf.concat([vec_qqp1, vec_qqp2, diff_qqp, mul_qqp], 1)

    nli = entailDense(catted_nli)
    qqp = semanticEquivalence(catted_qqp)
    manSim = manhatSim(vec_sst1, vec_sst2)

    model = tf.keras.Model(inputs=[sst1, sst2, nli1, nli2, qqp1, qqp2], outputs = [nli, qqp, manSim])
    model.summary()
    return model

def ValidationSiamese(transformerLayer, maximum, inputDim):
    sentence1 = tf.keras.Input(shape=(maximum,inputDim), name='sentence1', dtype=tf.float32)
    sentence2 = tf.keras.Input(shape=(maximum,inputDim), name='sentence2', dtype=tf.float32)
    target = tf.keras.Input(shape=(1),name='target', dtype=tf.float32)

    vector1 = transformerLayer(sentence1)
    vector2 = transformerLayer(sentence2)

    dotted = tf.math.multiply(vector1, vector2)
    dotted = tf.reduce_sum(dotted, axis=1, keepdims=True)
    sum1 = tf.math.sqrt(tf.reduce_sum(tf.math.square(vector1), axis=1, keepdims=True))
    sum2 = tf.math.sqrt(tf.reduce_sum(tf.math.square(vector2), axis=1, keepdims=True))
    denom = tf.math.multiply(sum1, sum2)
    cosineSim = tf.math.divide(dotted, denom)
    sim = tf.abs(tf.subtract(cosineSim, target))
    model = tf.keras.Model(inputs=[sentence1, sentence2, target], outputs = [sim])
    model.summary()
    return model

def ParagraphSiamese(transformerLayer, maximum):
    paragraph = tf.keras.Input(shape=(maximum,transformerLayer.outDim), name='paragraph', dtype=tf.float32)
    queries = tf.keras.Input(shape=(maximum,transformerLayer.outDim), name='queries', dtype=tf.float32)

    vector = transformerLayer(paragraph)
    matrix = transformerLayer(queries)

    dotted = tf.math.multiply(vector, matrix)
    dotted = tf.reduce_sum(dotted, axis=1, keepdims=True)
    sum1 = tf.math.sqrt(tf.reduce_sum(tf.math.square(vector), axis=1, keepdims=True))
    sum2 = tf.math.sqrt(tf.reduce_sum(tf.math.square(matrix), axis=1, keepdims=True))
    denom = tf.math.multiply(sum1, sum2)
    cosineSim = tf.math.divide(dotted, denom)
    model = tf.keras.Model(inputs=[paragraph, queries], outputs = [cosineSim])
    model.summary()
    return model
