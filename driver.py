import sifEmbed as sif
import pickle
import numpy as np
import nltk
from scipy import spatial
import csv
from scipy.stats import pearsonr as pearson
import tensorflow as tf
from DAN import Classifier

vocabDict = pickle.load(open('sif_vocabDict.pckl', "rb"))
vecLookup = pickle.load(open('sif_vecLookup.pckl', "rb"))
freqLookup = pickle.load(open('sif_freqLookup.pckl', "rb"))
print('loaded')


params = {
'alpha': 1e-3,
'dim':300,
'vocabulary': vocabDict,
'vecLookup':vecLookup,
'freqLookup':freqLookup
}

paragraphs = [['This is a car', 'I love my car', 'my car is the best thing in the world'],['This is somewhat a pretty good, actually the best, not to mention the most loving, also the best, car.',
'I adore car, which is the most fanciest things that that that that that that that that that that that that that that that that that that that that that I cannot mention in my own words that is one of things that I do not know, which is my car is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is is.',
'my car which is one of the best things in all of the cars one of most things i have and it is the best things but I do not.'
],['is an the all of most thing that I eat which I see the most things love I one of the an the is all of one the that thing.',
'eat love my an I this that this an an an an the love an an an an an an an the the the the the thing things things love to one of most']]

a = sif.sifEmbedParagraphs(paragraphs, params)

s1 = tf.placeholder(tf.float32, shape=(None, 300))
s2 = tf.placeholder(tf.float32, shape=(None, 300))

model = Classifier()
output = model.call(s1,s2)

saver = tf.train.Saver()
session = tf.Session()

saver.restore(session, "./paragraphFilter/")


print(session.run(output, feed_dict={s1:np.expand_dims(a[0], axis=0), s2:np.expand_dims(a[1], axis=0)}))
print(session.run(output, feed_dict={s1:np.expand_dims(a[1], axis=0), s2:np.expand_dims(a[2], axis=0)}))
print(session.run(output, feed_dict={s1:np.expand_dims(a[0], axis=0), s2:np.expand_dims(a[2], axis=0)}))


#
# sentences = []
#
# questions = []
#
# batch = []
#
# scores = []
#
# qscores = []
#
# with open('./datasets/stsbenchmark/sts-test.csv','r') as f:
#     lines = f.readlines(204800)
#     while(lines):
#         for line in lines:
#             extractedData = line.strip('\n').split('\t')
#             score = float(extractedData[4]) / 5.0 # normalization
#
#             scores.append(score)
#
#             s1 = extractedData[5]
#             s2 = extractedData[6]
#
#             sentences.append(s1)
#             sentences.append(s2)
#         lines = f.readlines(204800)
#
#
# with open("./datasets/questions.csv", newline='') as f:
#
#     reader = csv.reader(f, delimiter=',')
#     count = 0
#     for row in reader:
#         if(count > 0):
#             data = row
#             for i in range(len(data)):
#                 data[i] = data[i].replace("\"", '')
#                 data[i] = data[i].replace("\'", '')
#
#
#             s1 = data[3]
#             s2 = data[4]
#
#             # sentences.append(s1)
#             # sentences.append(s2)
#             if(data[5] != 'is_duplicate'):
#                 questions.append(s1)
#                 questions.append(s2)
#                 qscores.append(float(data[5]))
#                 # sentences.append(s1)
#                 # sentences.append(s2)
#                 # scores.append(float(data[5]))
#
#             if(count > 10000):
#                 break
#         count += 1
#
# batch.extend(sentences)
# batch.extend(questions)
#
# sentences.append('')
#
# print('computing vectors')
#
# vectors = sif.sifEmbed(sentences, params)[0]
#
# print(vectors[len(vectors)-1])
# print(vectors[0])
#
# qvectors = sif.sifEmbed(questions, params)[0]
#
#
#
# # batches = sif.sifEmbed(batch, params)
#
# print('complete')
#
# x = np.zeros(len(scores), dtype = float)
# y = np.zeros(len(scores), dtype = float)
#
# tot_acc = 0.0
#
# for i in range(len(scores)):
#     similarity = 1 - spatial.distance.cosine(vectors[i*2], vectors[i*2+1])
#     # similarity = 1 - spatial.distance.cosine(batches[i*2], batches[i*2+1])
#     x[i] = similarity
#     y[i] = scores[i]
#     # print('similarity: ', similarity)
#     # print('score: ',i,' ', scores[i])
#
# print('sentence correlation: ', pearson(x,y))
#
# x = np.zeros(len(qscores), dtype = float)
# y = np.zeros(len(qscores), dtype = float)
#
# tot_acc = 0.0
#
# for i in range(len(qscores)):
#     # similarity = 1 - spatial.distance.cosine(batches[(i+len(scores))*2], batches[(i+len(scores))*2+1])
#     similarity = 1 - spatial.distance.cosine(qvectors[i*2], qvectors[i*2+1])
#     x[i] = similarity
#     y[i] = qscores[i]
#     # print('similarity: ', similarity)
#     # print('score: ', qscores[i])
#
# print('question correlation: ', pearson(x, y))
#
#
#
# # 'I am riding on a car',
# # 'I am on a train',
# # 'obama is president',
# # 'there is a guy on the horse',
# # 'you are riding the car',
# # 'he is on steroids',
# # 'the person is on serious medication and is very strong',
# # 'he is super bulked up with those pills',
# # 'come on, we are getting late',
# # 'someone is up for dinner, I see',
# # 'the fact that the internet thrives boggles me with maximum uncertainty',
# # 'one of the most important beliefs in computer programming is that you are always wrong',
# # 'this is one of the most weirdest experiences',
# # 'there is a cat on the floor dancing',
# # 'i like to dance',
# # 'the dog dances on the stage too',
