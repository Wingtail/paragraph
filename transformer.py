import tensorflow as tf
import numpy as np
import pickle
import time
import math


group = pickle.load(open("words.dat","rb"))


DW = 300
N_HEADS = 6
INPUT_SIZE = 300
LOOKUPTABLE = group[1]
BATCH_SIZE = 32


class Multihead(tf.layers.Layer):
    def __init__(self, dW, h, inSize, batch_size):
        super(Multihead,self).__init__()
        self.Q = tf.layers.Dense(dW, use_bias=False, name="Q")
        self.K = tf.layers.Dense(dW, use_bias=False, name="K")
        self.V = tf.layers.Dense(dW, use_bias=False, name="V")

        self.linear = tf.layers.Dense(dW, use_bias=False, name="outLinear")

        self.dk = tf.cast(dW/h, dtype=tf.int64) #assume that dW is divisible by h
        self.h = h
        self.dW = dW
        self.inSize = inSize

        self.batch_size = batch_size

        self.negInf = -1e9


    def splitHead(self, x):
        dk = (self.dW // self.h)
        x = tf.reshape(x, (-1, self.inSize, self.h, dk)) #(batch, row, head, dk)
        x = tf.transpose(x, (0,2,1,3)) #(batch, head, row, dk)
        return x

    def concatHead(self, x):
        x = tf.transpose(x, (0,2,1,3))
        x = tf.reshape(x, (-1, self.inSize, self.dW))
        return x

    def call(self, x, bias):
        #assume x is already casted

        #Linearly transform x into q, k, v matrices
        q = self.Q(x)
        k = self.K(x)
        v = self.V(x)

        #Split computed matrix into heads --> more efficient than tf.slice
        q = self.splitHead(q)
        k = self.splitHead(k)
        v = self.splitHead(v)

        dk = (self.dW // self.h)
        q *= dk ** -0.5 #prevent decimal overflow during dotproduct attention

        kT = tf.transpose(k, (0,1,3,2)) #transpose for dot product
        attention = tf.matmul(q,kT)

        attention += bias #prevent padding dimension to interfere with softmax computation
        softAttention = tf.nn.softmax(attention)

        z = tf.matmul(softAttention, v)
        z = self.concatHead(z)

        z = self.linear(z)

        return z

class Feedforward(tf.layers.Layer):
    def __init__(self, dW, inSize, filterSize=1200):
        super(Feedforward, self).__init__()
        self.linear1 = tf.layers.Dense(filterSize, use_bias=True, activation=tf.nn.relu, name='filterLayer') #Convolution of kernel size 1
        self.linear2 = tf.layers.Dense(dW, use_bias=True, name='outLayer')



    def call(self, x):
        x = self.linear1(x)
        ffw = self.linear2(x)
        return ffw


class Embedding(tf.layers.Layer):
    def __init__(self, wordEmbed, dW):
        super(Embedding,self).__init__()
        self.params = wordEmbed
        self.dW = dW

    def call(self, x):
        return tf.nn.embedding_lookup(self.params, x)

class EncodingBlock(tf.layers.Layer):
    def __init__(self, dW, dh, inSize, batchSize):
        super(EncodingBlock,self).__init__()
        self.multihead = Multihead(dW, dh, inSize, batchSize)
        self.ffw = Feedforward(dW, inSize)
        self.layerNorm1 = LayerNormalization(300)
        self.layerNorm2 = LayerNormalization(300)

    def call(self, x, bias):
        z = self.multihead(x, bias)
        z += x #residual connection
        z = self.layerNorm1(z)
        transformed = self.ffw(z)
        transformed += z #residual connection
        transformed = self.layerNorm2(transformed)
        return transformed

class LayerNormalization(tf.layers.Layer):
    def __init__(self, hidden_size):
        super(LayerNormalization, self).__init__()
        self.hidden_size = hidden_size
        self.scale = tf.Variable(tf.ones([self.hidden_size]), trainable=True)
        self.bias = tf.Variable(tf.ones([self.hidden_size]), trainable=True)

    def call(self, x, epsilon=1e-6):
        mean = tf.reduce_mean(x, axis=[-1], keepdims=True)
        variance = tf.reduce_mean(tf.square(x - mean), axis=[-1], keepdims=True)
        norm_x = (x - mean) * tf.rsqrt(variance + epsilon)
        return norm_x * self.scale + self.bias

class Transformer(object):
    def __init__(self, wordEmbed, dW, dh, inSize, batchSize, nBlocks):
        self.embed = Embedding(wordEmbed, dW)
        # self.layerNorm = LayerNormalization(dW)

        # self.conv1D = tf.layers.Conv1D(1, (dW//dh), strides=(dW//dh), padding='same', activation=tf.nn.relu)

        self.layers = []
        self.dW = dW
        self.inSize = inSize

        self.dense = tf.layers.Dense(dW, use_bias=True, activation=tf.nn.tanh)

        for _ in range(nBlocks):
            encoding = EncodingBlock(dW, dh, inSize, batchSize)
            self.layers.append(encoding)

    def call(self, x):
        bias = self.get_padding_bias(x)
        x = self.embed(x)
        x = tf.cast(x, dtype=tf.float32)

        x += self.get_position_encoding(self.inSize, self.dW)

        for i in range(len(self.layers)):
            block = self.layers[i]
            x = block(x, bias)

        vector = tf.math.reduce_sum(x, axis=1)
        vector /= tf.math.sqrt(300)
        # quotient = tf.squeeze(bias)
        # quotient = tf.reduce_sum((quotient / -1e9), axis=1)
        #
        # vector /= tf.math.sqrt(quotient)
        # x = tf.layers.flatten(x)
        # x = self.conv1D(x)

        #vector = self.layerNorm(x)

        # vector = self.dense(vector)

        # vector = tf.reduce_sum(x, axis=1)
        #
        # vector /= 300

        return vector
    def get_position_encoding(self, length, hidden_size, min_timescale=1.0, max_timescale=1.0e4):
      """Return positional encoding.
      Calculates the position encoding as a mix of sine and cosine functions with
      geometrically increasing wavelengths.
      Defined and formulized in Attention is All You Need, section 3.5.
      Args:
        length: Sequence length.
        hidden_size: Size of the
        min_timescale: Minimum scale that will be applied at each position
        max_timescale: Maximum scale that will be applied at each position
      Returns:
        Tensor with shape [length, hidden_size]
      """
      # We compute the positional encoding in float32 even if the model uses
      # float16, as many of the ops used, like log and exp, are numerically unstable
      # in float16.
      position = tf.cast(tf.range(length), tf.float32)
      num_timescales = hidden_size // 2
      log_timescale_increment = (
          math.log(float(max_timescale) / float(min_timescale)) /
          (tf.cast(num_timescales, tf.float32) - 1))
      inv_timescales = min_timescale * tf.exp(
          tf.cast(tf.range(num_timescales), tf.float32) * -log_timescale_increment)
      scaled_time = tf.expand_dims(position, 1) * tf.expand_dims(inv_timescales, 0)
      signal = tf.concat([tf.sin(scaled_time), tf.cos(scaled_time)], axis=1)
      return signal

    def get_padding(self, x, padding_value=0, dtype=tf.float32):
      with tf.name_scope("padding"):
        return tf.cast(tf.equal(x, padding_value), dtype)


    def get_padding_bias(self, x):
      """
      Args:
        x: int tensor with shape [batch_size, length]
      Returns:
        Attention bias tensor of shape [batch_size, 1, 1, length].
      """
      with tf.name_scope("attention_bias"):
        padding = self.get_padding(x)
        attention_bias = padding * -1e9
        attention_bias = tf.expand_dims(
            tf.expand_dims(attention_bias, axis=1), axis=1)
      return attention_bias

def cosineSim(vec1, vec2):
    numeral = tf.reduce_sum(vec1*vec2, axis=1, keepdims=True)

    vec1N = tf.reduce_sum(tf.math.square(vec1), axis=1, keepdims=True)
    vec1N = tf.math.sqrt(vec1N)

    vec2N = tf.reduce_sum(tf.math.square(vec2), axis=1, keepdims=True)
    vec2N = tf.math.sqrt(vec2N)

    denom = vec1N*vec2N

    return numeral/denom



def manhattanSim(vec1, vec2):
    diff = vec1 - vec2
    absDiff = tf.math.abs(diff)
    return absDiff


def nli(vec1, vec2, denseLayers):
    mul_nli = tf.math.multiply(vec1, vec2)
    diff_nli = tf.math.abs(tf.math.subtract(vec1, vec2))
    catted_nli = tf.concat([vec1, vec2, mul_nli, diff_nli], 1)

    for denseLayer in denseLayers:
        catted_nli = denseLayer(catted_nli)
    return catted_nli

def qqp(vec_qqp1, vec_qqp2):
    #mul_qqp = tf.math.multiply(vec_qqp1, vec_qqp2)
    diff_qqp = tf.math.abs(tf.math.subtract(vec_qqp1, vec_qqp2))
    #catted_qqp = tf.concat([vec_qqp1, vec_qqp2, diff_qqp, mul_qqp], 1)
    qqpOut = tf.layers.dense(diff_qqp, 2)
    return qqpOut

def accuracy(prediction, label):
    diff = tf.math.abs(prediction - label)
    acc = tf.where(tf.math.less(diff, 0.05), tf.ones([tf.shape(prediction)[0], tf.shape(prediction)[1]]), tf.zeros([tf.shape(prediction)[0], tf.shape(prediction)[1]]))
    denom = tf.cast(tf.shape(acc)[0], tf.float32)
    sum = tf.reduce_sum(acc, axis=0)
    return sum

def nliAcc(prediction, label):
    pred = tf.math.argmax(prediction, axis=1)
    lab = tf.math.argmax(label, axis=1)
    return tf.reduce_sum(tf.cast(tf.equal(pred, lab), tf.float32), axis=0)

def mseLoss(y, target):
    return tf.losses.mean_squared_error(target, y)

def crossEntropy(y, target):
    return tf.losses.sigmoid_cross_entropy(target, y, label_smoothing=0.05)
