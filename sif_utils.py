import numpy as np
from tqdm import tqdm
import pickle

def get_positionEmbedding(max_length, dim, max_freq = 1e5):
    '''
    Generates position embeddings according to method proposed by
    Attention is All You Need: 3.5

    Arg: max_length --> max length of the sequence
        dim --> dimension of each word vector of the sequence

    out: matrix with shape (max_length, dim) with position embeddings

    '''
    #Casting

    expSin = np.expand_dims(2 * np.arange(0.0,float(dim),2) / float(dim), axis=1)
    expCos = np.expand_dims(2 * np.arange(1.0,float(dim),2) / float(dim), axis=1)

    position = np.expand_dims(np.expand_dims(np.arange(float(max_length)), axis=1), axis=1)

    factorSin = np.sin(position / np.power(max_freq, expSin))
    factorCos = np.cos(position / np.power(max_freq, expCos))

    concat = np.concatenate((factorSin, factorCos), axis=2)

    posEmbedding = np.reshape(concat, (max_length, dim))
    return posEmbedding

def makeWordDic(freqDirectory, vocabDirectory, BUFFER = 204800, minFreq = 200):

    vocabDict = {}

    N = 0.0

    rows = 0
    cols = 0

    ID = 1

    with open(vocabDirectory, 'r') as f:
        lines = f.readlines(BUFFER)
        with tqdm(total=(1e5 * 4)) as pbar:
            while(lines):
                for line in lines:
                    if(rows == 0):
                        line = line.rstrip()
                        splitPoint = line.index(' ')
                        wordVec = np.fromstring(line[splitPoint+1:], dtype='float32', sep=' ')
                        cols = wordVec.shape[0]
                    rows += 1
                    pbar.update(1)
                lines = f.readlines(BUFFER)

    wordVeclookup = np.zeros((rows+1,cols), dtype=float)
    wordFreqlookup = np.zeros((rows+1,1), dtype=float)

    wordFreqDict = {}

    with open(vocabDirectory, 'r') as f:
        lines = f.readlines(BUFFER)
        with tqdm(total=(1e5 * 4)) as pbar:
            while(lines):
                for line in lines:
                    line = line.rstrip()
                    splitPoint = line.index(' ')

                    wordVec = np.fromstring(line[splitPoint+1:], dtype='float32', sep=' ')
                    word = line[:splitPoint]

                    vocabDict[word] = ID
                    wordVeclookup[ID] = wordVec
                    ID += 1
                    pbar.update(1)
                lines = f.readlines(BUFFER)

    with open(freqDirectory, 'r') as f:
        lines = f.readlines(BUFFER)
        with tqdm(total=(1e5 * 4)) as pbar:
            while(lines):
                for line in lines:
                    line = line.rstrip()
                    splitPoint = line.index(' ')
                    if(line[:splitPoint] in vocabDict):
                        freq = float(line[splitPoint:])
                        wordFreqlookup[vocabDict[line[:splitPoint]]] = freq
                        N += freq
                    pbar.update(1)
                lines = f.readlines(BUFFER)

    for key in vocabDict.keys():
        if(wordFreqlookup[vocabDict[key]][0] == 0.0):
            wordFreqlookup[vocabDict[key]] = minFreq
            N += minFreq

    wordFreqlookup /= N

    wordFreqlookup[0] = 1e9

    print('complete')

    pickle.dump(vocabDict, open("sif_vocabDict.pckl", "wb"))
    pickle.dump(wordVeclookup, open("sif_vecLookup.pckl", "wb"))
    pickle.dump(wordFreqlookup, open("sif_freqLookup.pckl", "wb"))

    print('saved vocabDict file')

    return vocabDict


#makeWordDic('./datasets/enwiki_vocab_min200.txt', './datasets/glove.6B/glove.6B.300d.txt')

# print(get_positionEmbedding(300,300))
