import tensorflow as tf
import numpy as np
import transformer
import pickle
import __dataset_manager__ as manager


DW = 300
N_HEADS = 6
INPUT_SIZE = 300
BATCH_SIZE = 10

#Test Data
lookuptable = pickle.load(open("lookuptable.pckl", "rb"))

train_dataset = manager.getDataset('snliTrain.tfrecord', batchSize = BATCH_SIZE, shuffle=True, repeat=True)

train_iterator = train_dataset.make_one_shot_iterator()
train_s1, train_s2, train_label = train_iterator.get_next()

vectorModel = transformer.Transformer(lookuptable, DW, N_HEADS, INPUT_SIZE, BATCH_SIZE, 3)

# modelLayer1 = transformer.EncodingBlock(DW, N_HEADS, INPUT_SIZE, BATCH_SIZE)
# modelLayer2 = transformer.EncodingBlock(DW, N_HEADS, INPUT_SIZE, BATCH_SIZE)
# modelLayer3 = transformer.EncodingBlock(DW, N_HEADS, INPUT_SIZE, BATCH_SIZE)
#Dataflow model

train_vec1 = vectorModel.call(train_s1)
train_vec2 = vectorModel.call(train_s2)

# cosine_similarity = transformer.cosineSim(train_vec1, train_vec2)

kernel = tf.Variable(tf.random_normal([DW//N_HEADS]))
nliLayer = tf.layers.Dense(3, use_bias=True, activation=tf.nn.softmax)

nliLogits = transformer.nli(train_vec1, train_vec2, nliLayer, convKernel=kernel)

# nli_loss = transformer.crossEntropy(nliLogits, train_label)
# nli_acc = transformer.nliAcc(nliLogits, train_label)

# v1 = modelLayer1(s1)
# v1 = modelLayer2(v1)
# v1 = modelLayer3(v1)
#
# v2 = modelLayer1(s2)
# v2 = modelLayer2(v2)
# v2 = modelLayer3(v2)
#
#
# sim = transformer.cosineSim(v1, v2)
#
# # dist = tf.losses.cosine_distance(v1, v2, axis=2)
#
# acc = accuracy(sim, label)

init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    logits = sess.run(nliLogits)
    print('logits: ', logits)
    print('logits shape: ', logits.shape)


    # for i in xs:
    #     print(i)
    #     print('#######################')
