import tensorflow as tf

class LayerNormalization(tf.layers.Layer):
    def __init__(self, hidden_size):
        super(LayerNormalization, self).__init__()
        self.hidden_size = hidden_size
        self.scale = tf.Variable(tf.ones([self.hidden_size]), trainable=True)
        self.bias = tf.Variable(tf.ones([self.hidden_size]), trainable=True)

    def call(self, x, epsilon=1e-6):
        mean = tf.reduce_mean(x, axis=[-1], keepdims=True)
        variance = tf.reduce_mean(tf.square(x - mean), axis=[-1], keepdims=True)
        norm_x = (x - mean) * tf.rsqrt(variance + epsilon)
        return norm_x * self.scale + self.bias

class Classifier(object):
    def __init__(self):
        # self.dense3 = tf.layers.Dense(512, activation=tf.nn.relu, kernel_regularizer=tf.contrib.layers.l2_regularizer(0.001))
        # self.dense4 = tf.layers.Dense(256, activation=tf.nn.relu, kernel_regularizer=tf.contrib.layers.l2_regularizer(0.001))
        # self.dense5 = tf.layers.Dense(128, activation=tf.nn.relu, kernel_regularizer=tf.contrib.layers.l2_regularizer(0.001))
        # self.dense6 = tf.layers.Dense(64, activation=tf.nn.sigmoid, kernel_regularizer=tf.contrib.layers.l2_regularizer(0.001))
        # self.dense7 = tf.layers.Dense(1, activation=tf.nn.sigmoid)

        # self.dense1 = tf.layers.Dense(1600, activation=tf.nn.relu)
        # self.dense2 = tf.layers.Dense(1024, activation=tf.nn.relu)
        self.dense1 = tf.layers.Dense(512, activation=tf.nn.elu)
        self.dense2 = tf.layers.Dense(512, activation=tf.nn.elu)
        self.dense3 = tf.layers.Dense(512, activation=tf.nn.elu)
        # self.dense5 = tf.layers.Dense(512, activation=tf.nn.elu)
        # self.dense6 = tf.layers.Dense(512, activation=tf.nn.elu)
        self.dense4 = tf.layers.Dense(1, activation=tf.nn.sigmoid)
        self.lnorm1 = LayerNormalization(512)
        self.lnorm2 = LayerNormalization(512)
        # self.bnorm = tf.layers.BatchNormalization()


    def call(self, x1, x2, training=False):
        multiplied = tf.math.multiply(x1, x2)
        difference = tf.math.abs(tf.math.subtract(x1, x2))
        concat = tf.concat([x1, x2, difference, multiplied], 1)

        # out = self.dense1(concat)
        # out = self.dense2(out)
        # if(training == True):
        #     #concat = self.bnorm(concat)
        #     concat = tf.nn.dropout(concat, rate=0.1)

        out1 = self.dense1(concat)
        out2 = self.dense2(out1)
        #out2 += out1
        #out2 = self.lnorm1(out2)
        out3 = self.dense3(out2)
        #out3 += out2
        #out3 = self.lnorm2(out3)
        out4 = self.dense4(out3)

        return out4
