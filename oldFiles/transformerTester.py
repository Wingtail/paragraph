import tensorflow as tf
from transformerBlock import Transformer, ParagraphSiamese, Siamese


#a = tf.constant([[[1.0,2.0,4.5],[1.,5.,6.],[5.,3.,5.7]],[[0.,0.,0.],[0.,0.,0.],[5.,3.,5.7]],[[1.,2.,3.],[0.,0.,0.],[5.,3.,5.7]],[[1.,2.,3.],[0.,0.,0.],[5.,3.,5.7]],[[1.,2.,3.],[0.,0.,0.],[5.,3.,5.7]]])
a = tf.constant([[[3.0,6.0,4.5],[1.,2.,4.],[7.,111.,8.7]],[[3.,0.,0.],[0.,6.,0.],[5.,2.,5.7]],[[1.,2.,3.],[0.,0.,0.],[1.,3.,5.7]]])
b = tf.constant([[[4.0,6.0,1.5],[4.,3.,4.],[1.,131.,4.7]],[[3.,5.,0.],[6.,6.,0.],[8.,7,2.7]],[[2.,4.,6.],[0.,0.,0.],[2.,6.,11.4]]])
c = tf.constant([[[3.0,6.0,4.5],[1.,2.,4.],[7.,111.,8.7]]])
#special = tf.constant([[[1.,2.,3.,4.],[0.,0.,0.,0.],[0.,0.,0.,0.]]])

# com1 = tf.constant([[1.,2.,3.,4.],[5.,6.,7.,8.],[1.,1.,1.,1.],[2.,2.,2.,2.],[4.,4.,4.,4.]])
# com2 = tf.constant([[2.,2.,2.,2.]])
#
# dotted = tf.math.multiply(com1, com2)
# dotted = tf.reduce_sum(dotted, axis=1, keepdims=True)
# sum1 = tf.math.sqrt(tf.reduce_sum(tf.math.square(com1), axis=1, keepdims=True))
# sum2 = tf.math.sqrt(tf.reduce_sum(tf.math.square(com2), axis=1, keepdims=True))
# denom = tf.math.multiply(sum1, sum2)
# cosineSim = tf.math.divide(dotted, denom)

transformer = Transformer(3,1,3)

transformer.compile(optimizer=tf.train.AdamOptimizer(1e-6), loss='mse', metrics=['acc'])

sentence = Siamese(transformer, 3)
paragraph = ParagraphSiamese(transformer, 3)

print(sentence.predict({'sentence1':a, 'sentence2':b}, steps=1))
#print(paragraph.predict({'paragraph':c, 'queries':a}, steps=1))


# with tf.Session() as session:
#     dotted, sum1, sum2, denom, sim = session.run([dotted, sum1, sum2, denom, cosineSim])
#     print(dotted)
#     print(sum1)
#     print(sum2)
#     print(denom)
#     print(sim)
#     # print(a)
#     # print(b)
#     # print(c)
#     # print(d)
#     # print(e)
#     # print(pos)
