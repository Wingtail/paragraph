import tensorflow as tf
import numpy as np


def _parse(record):
    # Don't forget to replace the shape with the actual value
    features = {
        'sentence1': tf.FixedLenFeature([300], tf.int64),
        'sentence2': tf.FixedLenFeature([300], tf.int64),
        'label': tf.FixedLenFeature([1], tf.float32)
    }
    parsed_example = tf.parse_single_example(record, features)
    return parsed_example

graph = tf.Graph()
with graph.as_default():
    # Create a dataset
    dataset = tf.data.TFRecordDataset('trainSentencePairs.tfrecord')
    dataset = dataset.map(_parse)
    #dataset = dataset.repeat()
    dataset = dataset.batch(64)
    iterator = dataset.make_one_shot_iterator()
    next_element = iterator.get_next()

count = 0

with tf.Session(graph=graph) as sess:
    while True:
        try:
            element = sess.run(next_element)
            print(count)
            print(element['sentence1'].shape)
            count += 1
        except tf.errors.OutOfRangeError:
            break
