import numpy as np
from tensorflow.python.keras.utils.data_utils import Sequence
import pickle
import gzip
import blosc
import random

grouping = pickle.load(open("words.dat","rb"))
lookuptable = grouping[1]

def cacheArray(dataDirectory, saveDirectory):
    with gzip.open(dataDirectory,'r') as file:
        sentencePairs = pickle.load(file)
    print("shuffling")
    sentencePairs = list(sentencePairs)
    random.shuffle(sentencePairs)
    print("shuffled")
    sentencePairs = list(sentencePairs)
    dim = (500,300)
    batchSize = 64
    batchNum = int(len(sentencePairs)/batchSize)
    if(len(sentencePairs) % batchSize != 0):
        batchNum += 1

    for index in range(batchNum):
        start = index * batchSize
        end = (index+1) * batchSize

        if(end > len(sentencePairs)):
            end = len(sentencePairs)

        sent1 = np.zeros((end-start, *dim))
        sent2 = np.zeros((end-start, *dim))
        y = np.zeros((end-start), dtype=float)

        for i in range(start, end):
            for j in range(len(sentencePairs[i][0])):
                sent1[i-start][j] = lookuptable[sentencePairs[i][0][j]]
            for j in range(len(sentencePairs[i][1])):
                sent2[i-start][j] = lookuptable[sentencePairs[i][1][j]]
            y[i-start] = sentencePairs[i][2]

        sentDirectory = saveDirectory+'sentPair_'+str(index)

        sent1String = blosc.compress(sent1.tostring(), typesize=8, cname='lz4')
        sent2String = blosc.compress(sent2.tostring(), typesize=8, cname='lz4')
        labelString = blosc.compress(y.tostring(), typesize=8, cname='lz4')

        pair = [sent1String, sent2String, labelString]

        with open(sentDirectory, "wb") as file:
            pickle.dump(pair, file)

        print(index/float(batchNum) * 100, '% complete')

    with open(saveDirectory+'info.txt', 'w') as file:
        file.write(str(batchNum))
        file.write('\n')


#cacheArray('trainSentencePairs.pgz', 'data/training/')
#cacheArray('valiSentencePairs.pgz', 'data/validation/')
