import tensorflow as tf
import pickle
from tensorflow import contrib
import numpy as np
from transformerBlock import Transformer, ParagraphSiamese
import random
from datetime import datetime
from tensorflow import keras
from tensorflow.python.keras.callbacks import TensorBoard
from paragraphDataGen import DataGenerator
import gzip

f = gzip.GzipFile('processedData/validation_paragraphBatch.npy.gz', "r")
vali_paragraphBatch = np.load(f)
f = gzip.GzipFile('processedData/validation_queryBatch.npy.gz', "r")
vali_queryBatch = np.load(f)

paragraphs = pickle.load(open("processedData/validation_paragraphFile","rb"))
queries = pickle.load(open("processedData/validation_queryFile","rb"))

vali_mapping = pickle.load(open('processedData/validation_mapFile',"rb"))

vali_paragraphBatch = vali_paragraphBatch.astype(np.float32)
vali_queryBatch = vali_queryBatch.astype(np.float32)

transformer = Transformer(3, 6, 300)
transformer.load_weights('./checkpoint/paragraphTransformer')
model = ParagraphSiamese(transformer, vali_paragraphBatch.shape[1])
model.compile(optimizer=tf.train.AdamOptimizer(0.0001), loss='mse', metrics=['acc'])

# logdir = "logs/fit/" + datetime.now().strftime("%Y%m%d-%H%M%S")
# tensorboardCallback = keras.callbacks.TensorBoard(log_dir=logdir)

#go through queries from query file

#print(vali_paragraphBatch.shape)

for query in queries:
    queryIndex = queries.index(query)
    queryVector = vali_queryBatch[queryIndex]
    #print(queryVector)
    print(query)
    qs = np.zeros((vali_paragraphBatch.shape[0], vali_paragraphBatch.shape[1], vali_paragraphBatch.shape[2])).astype(np.float32)

    for i in range(len(qs)):
        qs[i][0] = queryVector
    #print(queries)
    a = model.predict({'paragraph':vali_paragraphBatch, 'queries':qs}) #predict something
    a = a.tolist()
    print(paragraphs[a.index(max(a))])
    #break
