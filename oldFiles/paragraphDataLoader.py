import json
import pickle
import nltk


#validation

def loadParagraph(dir, saveExtension):
    paragraphData = []
    queryData = []

    mapping = {} #paragraph index:[query indices]

    paragraphIndex = 0
    queryIndex = 0
    grouping = pickle.load(open("words.dat", "rb"))

    vocabulary = grouping[0]
    lookuptable = grouping[1]
    with open(dir) as json_file:
        data = json.load(json_file)

        for i in range(len(data['data'])):
            for j in range(len(data['data'][i]['paragraphs'])):
                paragraphs = data['data'][i]['paragraphs'][j]

                paragraph = paragraphs['context']
                questions = paragraphs['qas']

                sentences = nltk.sent_tokenize(paragraph.lower())
                if(len(sentences) > 0 and len(questions) > 0):
                    q = []
                    for k in range(len(questions)):
                        words = nltk.word_tokenize(questions[k]['question'].lower())
                        words = [word for word in words if word in vocabulary]
                        if(len(words) > 0):
                            q.append(words)
                    if(len(q) > 0):
                        paragraphData.append(sentences) #raw text data. Need text filtering
                        mapping[str(paragraphIndex)] = [] #Initialize paragraph index

                        for k in range(len(q)):
                            #print(q)
                            queryData.append(q[k])
                            mapping[str(paragraphIndex)].append(queryIndex) #Have which query indices relate to the paragraph

                            queryIndex += 1
                        print(float(i)/len(data['data']))
                        paragraphIndex += 1

    #print(data['data'][0]['paragraphs'][0]['qas'][0])

    print('Total # of Queries: ', queryIndex+1)
    print('Total # of Paragraphs: ', paragraphIndex+1)

    paragraphFile = open("processedData/"+saveExtension+"paragraphFile","wb")
    queryFile = open("processedData/"+saveExtension+"queryFile","wb")
    mapFile = open("processedData/"+saveExtension+"mapFile","wb")

    pickle.dump(paragraphData, paragraphFile)
    paragraphFile.close()
    pickle.dump(queryData, queryFile)
    queryFile.close()
    pickle.dump(mapping,mapFile)
    mapFile.close()

loadParagraph('./datasets/train-v2.0.json', '')
loadParagraph('./datasets/dev-v2.0.json', 'validation_')
