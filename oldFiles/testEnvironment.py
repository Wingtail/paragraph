import tensorflow as tf
import numpy as np
import pickle
import time

tf.enable_eager_execution()
tfe = tf.contrib.eager

#creating embedding

#analyze size of word.dat

group = pickle.load(open("words.dat","rb"))

lookuptable = group[1]

print(lookuptable.shape[0])

#embeddingMatrix = tf.Variable(tf.constant(0.0,shape=list(lookuptable.shape)), trainable=False, name='embeddingMatrix')

#initialize dummy dataset --> 1 data-point entry test


class Multihead(tf.layers.Layer):
    def __init__(self, dW, h, inSize):
        super(Multihead,self).__init__()
        self.Q = tfe.Variable(tf.random_normal([dW, dW]), name="Q")
        self.K = tfe.Variable(tf.random_normal([dW, dW]), name="K")
        self.V = tfe.Variable(tf.random_normal([dW, dW]), name="V")

        self.activate = tfe.Variable(tf.random_normal([dW, 1]), name="activate")

        self.linear = tfe.Variable(tf.random_normal([dW, dW]), name="linear")

        self.dk = int(dW/h) #assume that dW is divisible by h
        self.h = h
        self.dW = dW
        self.inSize = inSize

        self.negInf = -1e9

        self.trainVars = [self.Q, self.K, self.V, self.linear]


    def splitHead(self, x):
        x = tf.reshape(x, (-1, self.inSize, self.h, self.dk)) #(batch, row, head, dk)
        x = tf.transpose(x, (0,2,1,3)) #(batch, head, row, dk)
        return x

    def concatHead(self, x):
        x = tf.transpose(x, (0,2,1,3))
        x = tf.reshape(x, (-1, self.inSize, self.dW))
        return x

    def attMask(self, k):
        #requires k that has already split heads
        mask = tf.expand_dims(tf.reduce_sum(tf.math.abs(k),axis=-1), axis=2)
        attMask = tf.where(tf.equal(mask, 0.0), tf.ones(mask.shape, tf.float32), tf.zeros(mask.shape, tf.float32))

        return attMask


    def call(self, x):
        x = tf.cast(x, dtype=tf.float32) #converting float64 to float32

        #assume x is already casted

        #Linearly transform x into q, k, v matrices
        q = tf.matmul(x, self.Q)
        k = tf.matmul(x, self.K)
        v = tf.matmul(x, self.V)

        #Split computed matrix into heads --> more efficient than tf.slice
        q = self.splitHead(q)
        k = self.splitHead(k)
        v = self.splitHead(v)

        mask = self.attMask(k) #generate mask
        q /= tf.math.sqrt(float(self.dk)) #prevent decimal overflow during dotproduct attention

        kT = tf.transpose(k, (0,1,3,2)) #transpose for dot product
        attention = tf.matmul(q,kT)

        attention += (mask * self.negInf) #prevent padding dimension to interfere with softmax computation
        softAttention = tf.nn.softmax(attention)

        z = tf.matmul(softAttention, v)
        z = self.concatHead(z)

        z = tf.matmul(z, self.linear)

        return z

class Feedforward(tf.layers.Layer):
    def __init__(self, dW, inSize):
        super(Feedforward, self).__init__()
        self.linear1 = tfe.Variable(tf.random_normal([dW, dW]), name="linear1") #Convolution of kernel size 1
        self.linear2 = tfe.Variable(tf.random_normal([dW, dW]), name="linear2")
        self.bias1 = tfe.Variable(tf.zeros([inSize, dW]), name="bias1")
        self.bias2 = tfe.Variable(tf.zeros([inSize, dW]), name="bias2")

        self.trainVars = [self.linear1, self.linear2, self.bias1, self.bias2]

    def call(self, x):
        ffw = tf.matmul(x, self.linear1)
        ffw += self.bias1
        ffw = tf.nn.tanh(ffw, name='tanh')
        ffw = tf.matmul(ffw, self.linear2)
        ffw += self.bias2
        return ffw


class Embedding(tf.layers.Layer):
    def __init__(self, wordEmbed, dW):
        super(Embedding,self).__init__()
        self.params = wordEmbed
        self.dW = dW

    def call(self, x):
        return tf.nn.embedding_lookup(self.params, x)

class EncodingBlock(tf.layers.Layer):
    def __init__(self, dW, dh, inSize):
        super(EncodingBlock,self).__init__()
        self.multihead = Multihead(dW, dh, inSize)
        self.ffw = Feedforward(dW, inSize)
        self.trainVars = []
        self.trainVars.extend(self.multihead.trainVars)
        self.trainVars.extend(self.ffw.trainVars)

    def call(self, x):
        z = self.multihead(x)
        z += x #residual connection
        transformed = self.ffw(z)
        transformed += z #residual connection
        return transformed

class Transformer(object):
    def __init__(self, wordEmbed, dW, dh, inSize, nBlocks):
        self.embed = Embedding(wordEmbed, dW)
        self.trainVars = []
        self.layers = []
        for _ in range(nBlocks):
            encoding = EncodingBlock(dW, dh, inSize)
            self.layers.append(encoding)
            self.trainVars.extend(encoding.trainVars)


    def call(self, x):
        x = self.embed(x)
        x = tf.cast(x, dtype=tf.float32)

        for i in range(len(self.layers)):
            block = self.layers[i]
            print('x: ', x)
            x = block(x)

        vector = tf.reduce_sum(x, axis=1)
        return vector

def cosineSim(vec1, vec2):
    numeral = tf.reduce_sum(vec1*vec2, axis=1, keepdims=True)

    vec1N = tf.reduce_sum(tf.math.square(vec1), axis=1, keepdims=True)
    vec1N = tf.math.sqrt(vec1N)

    vec2N = tf.reduce_sum(tf.math.square(vec2), axis=1, keepdims=True)
    vec2N = tf.math.sqrt(vec2N)

    denom = vec1N*vec2N

    return numeral/denom

def nli(vec1, vec2):
    # mul_nli = tf.math.multiply(vec1, vec2)
    diff_nli = tf.math.abs(tf.math.subtract(vec1, vec2))
    # catted_nli = tf.concat([vec1, vec2, diff_nli, mul_nli], 1)
    # catted_nli = tf.nn.softmax(catted_nli)
    return diff_nli

def qqp(vec_qqp1, vec_qqp2):
    # mul_qqp = tf.math.multiply(vec_qqp1, vec_qqp2)
    diff_qqp = tf.math.abs(tf.math.subtract(vec_qqp1, vec_qqp2))
    # catted_qqp = tf.concat([vec_qqp1, vec_qqp2, diff_qqp, mul_qqp], 1)
    # catted_qqp = tf.nn.softmax(catted_qqp)
    return diff_qqp

lookup = group[1]

model_default = Transformer(lookup,300,4,300,3)

nliLinear = tfe.Variable(tf.random_normal([300,3]), name='nliLinear')
qqpLinear = tfe.Variable(tf.random_normal([300,2]), name='qqpLinear')


model_default.trainVars.extend([nliLinear, qqpLinear])

def loss(y, target):
    return tf.losses.mean_squared_error(target, y)

def crossEntropy(y, target):
    return tf.losses.softmax_cross_entropy(target, y)

def backward(x, target):
    optimizer = tf.train.AdamOptimizer(learning_rate=1e-6)
    with tf.GradientTape() as tape:
      sst1 = model_default.call(inputs['sst1'])
      sst2 = model_default.call(inputs['sst2'])

      nli1 = model_default.call(inputs['nli1'])
      nli2 = model_default.call(inputs['nli2'])

      qqp1 = model_default.call(inputs['qqp1'])
      qqp2 = model_default.call(inputs['qqp2'])

      sim = cosineSim(sst1, sst2)
      nliProd = nli(nli1, nli2)
      qqpProd = qqp(qqp1, qqp2)

      nliVals = tf.matmul(nliProd, nliLinear)
      qqpVals = tf.matmul(qqpProd, qqpLinear)

      nliVals = tf.nn.softmax(nliVals)
      qqpVals = tf.nn.softmax(qqpVals)

      current_loss = loss(sim, tf.expand_dims(target['similarity'], axis=1))
      current_loss += crossEntropy(nliVals, tf.expand_dims(target['entailment'],axis=0))
      current_loss += crossEntropy(qqpVals, tf.expand_dims(target['semantic'], axis=0))

      print('current loss: ', current_loss)

      # model_default.trainVars.extend(tf.trainable_variables()) #include the dense layers as well

      grads = tape.gradient(current_loss, model_default.trainVars)
      optimizer.apply_gradients(zip(grads, model_default.trainVars), global_step=tf.train.get_or_create_global_step())
      return current_loss

def accuracy(y, target):
    target = tf.cast(target, dtype=tf.float32)
    diff = tf.math.abs(y-target)
    acc = tf.where(tf.math.less(diff, 0.01), tf.ones(diff.shape, tf.float32), tf.zeros(diff.shape, tf.float32))
    accTot = tf.reduce_sum(acc, axis=0)
    accTot /= tf.cast(target.shape[0], dtype=tf.float32)

    return accTot



def parse(record):
    # Don't forget to replace the shape with the actual value
    features = {
        'nli1': tf.FixedLenFeature([300], tf.int64),
        'nli2': tf.FixedLenFeature([300], tf.int64),
        'sst1': tf.FixedLenFeature([300], tf.int64),
        'sst2': tf.FixedLenFeature([300], tf.int64),
        'qqp1': tf.FixedLenFeature([300], tf.int64),
        'qqp2': tf.FixedLenFeature([300], tf.int64),
        'similarity': tf.FixedLenFeature([1], tf.float32),
        'entailment': tf.FixedLenFeature([3], tf.float32),
        'semantic': tf.FixedLenFeature([2], tf.float32)
    }
    parsed_example = tf.parse_single_example(record, features)
    return ({'sst1':parsed_example['sst1'],'sst2':parsed_example['sst2'],'nli1':parsed_example['nli1'],'nli2':parsed_example['nli2'],'qqp1':parsed_example['qqp1'],'qqp2':parsed_example['qqp2']},
    {'similarity':parsed_example['similarity'],'entailment':parsed_example['entailment'],'semantic':parsed_example['semantic']})


trainBatch = 32
valiBatch = 100

with open('trainSentencePairs.txt','r') as file:
    trainCount = int(file.read())

with open('valiSentencePairs.txt','r') as file:
    valiCount = int(file.read())

if(trainCount % trainBatch != 0):
    trainCount = trainCount/trainBatch + 1
else:
    trainCount = trainCount/trainBatch

if(valiCount % valiBatch != 0):
    valiCount = valiCount/valiBatch + 1
else:
    valiCount = valiCount/valiBatch

dataset = tf.data.TFRecordDataset('trainSentencePairs.tfrecord')
# dataset = dataset.take(count=2)
dataset = dataset.map(parse)
# dataset = dataset.repeat()
dataset = dataset.shuffle(int(300*0.4) + 3 * trainBatch)
dataset = dataset.batch(trainBatch)
dataset = dataset.prefetch(buffer_size=32*10)
# training_iterator = dataset.make_one_shot_iterator()

# next = training_iterator.get_next()

# x2 = tf.cast(x2, dtype=tf.float32) #converting float64 to float32
#
# if(self.device is not None):
#     with tf.device('gpu:0' if self.device=='gpu' else 'cpu'):
#         y1 = self.multiHeadAttention(x1)
#         y2 = self.multiHeadAttention(x2)
#         sim = self.cosineSim(y1, y2)
#
# else:
#     # Leave choice of device to default
#     y1 = self.multiHeadAttention(x1)
#     y2 = self.multiHeadAttention(x2)
#     sim = self.cosineSim(y1, y2)
#
# return sim


NUM_EPOCHS = 1000

# Initialize model, letting device be selected by default during Eager Execution

print('trainVars: ', model_default.trainVars)
saver = tfe.Saver(model_default.trainVars)

for epoch in range(NUM_EPOCHS):
  loss_total = tfe.Variable(0, dtype=tf.float32)

  for inputs, outputs in dataset:
    sst1 = model_default.call(inputs['sst1'])
    sst2 = model_default.call(inputs['sst2'])

    nli1 = model_default.call(inputs['nli1'])
    nli2 = model_default.call(inputs['nli2'])

    qqp1 = model_default.call(inputs['qqp1'])
    qqp2 = model_default.call(inputs['qqp2'])

    sim = cosineSim(sst1, sst2)
    nliProd = nli(nli1, nli2)
    qqpProd = qqp(qqp1, qqp2)

    nliVals = tf.matmul(nliProd, nliLinear)
    qqpVals = tf.matmul(qqpProd, qqpLinear)

    nliVals = tf.nn.softmax(nliVals)
    qqpVals = tf.nn.softmax(qqpVals)

    # print('Accuracy similarity: ', accuracy(sim, outputs['similarity']))

    if(epoch % 10 == 0):
        print("Predictions similarity: ", sim)
        print("Target similarity: ", outputs['similarity'])
        print("--------------------------")
        print("Predictions entailment: ", nliVals)
        print("Target entailment: ", outputs['entailment'])
        print("--------------------------")
        print("Predictions semantic: ", qqpVals)
        print("Target semantic: ", outputs['semantic'])
        print("--------------------------")

        saver.save('checkpoint.cpt')


    # print('Accuracy similarity: ', accuracy(nliProd, outputs['entailment']))
    # print('Accuracy similarity: ', accuracy(sim, outputs['semantic']))
    loss_total = loss_total + backward(inputs, outputs)

  print('Epoch {} - Average MSE: {:.4f}'.format(epoch + 1, loss_total.numpy() / epoch))
time_taken = time.time() - time_start

print('\nTotal time taken for training (secoonds): {:.2f}'.format(time_taken))
