import numpy as np
from tensorflow.python.keras.utils.data_utils import Sequence
import random

class DataGenerator(Sequence):
    'Generates data for Keras'
    def __init__(self, queryBatch, paragraphBatch, mapping, dim=(32,32)):
        'Initialization'
        self.dim = dim
        self.batchNum = len(paragraphBatch)
        self.mapping = mapping
        self.queryBatch = queryBatch
        self.paragraphBatch = paragraphBatch
        self.negativeSample = 7
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return self.batchNum

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch

        # Generate data
        sent1, sent2, y = self.__data_generation(index)

        return {"paragraph":sent1, "queries":sent2}, y

    def on_epoch_end(self):
        return

    def __data_generation(self, index):
        'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
        # Initialization
         #Negative Sample Size

        queryIndices = self.mapping[str(index)]
        label = [1.0 for i in range(len(queryIndices))]

        labelSize = len(label)

        while len(queryIndices) < (labelSize+self.negativeSample):
            neg = random.randrange(0,len(self.queryBatch),1)
            if(neg not in queryIndices):
                queryIndices.append(neg)
                label.append(0.0)

        shuffle = random.sample(range(0,len(queryIndices)), len(queryIndices))

        queryIndices = [queryIndices[i] for i in shuffle]
        label = [label[i] for i in shuffle]

        #print("LABEL: ",label)

        batch = np.zeros((labelSize+self.negativeSample, self.paragraphBatch.shape[1], self.paragraphBatch.shape[2]))
        queries = np.zeros((labelSize+self.negativeSample, self.paragraphBatch.shape[1], self.paragraphBatch.shape[2])).astype(np.float32)
        labels = np.zeros(labelSize+self.negativeSample)

        for i in range(len(label)):
            labels[i] = label[i]

        for i in range(len(batch)):
            batch[i] = self.paragraphBatch[index]

        for i in range(len(queries)):
            queries[i][0] = self.queryBatch[queryIndices[i]]

        return batch, queries, labels
