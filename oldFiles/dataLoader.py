import tensorflow as tf
import json_lines
import re
import numpy as np
import random
import csv
import pickle
import nltk
import gzip
import pandas as pd
from tensorflow.keras.preprocessing.sequence import pad_sequences
#import chars2vec

csvFile = pd.read_csv("./datasets/stsbenchmark/sts-train.csv", error_bad_lines=False)
print(csvFile.head())
