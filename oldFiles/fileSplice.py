import gzip
import pickle
from tensorflow.keras.preprocessing.sequence import pad_sequences

with gzip.open('train1.pgz','r') as file:
    sent1 = pickle.load(file)
    sent1 = pad_sequences(sent1, 300, padding='post')

with gzip.open('train2.pgz','r') as file:
    sent2 = pickle.load(file)
    sent2 = pad_sequences(sent2, 300, padding='post')

with gzip.open('trainLabel.pgz','r') as file:
    labels = pickle.load(file)

partition1 = []
partition2 = []
partitionLabel = []

partitionSize = 300000

i = 0

while i < len(sent1):
    partition1.append(sent1[i:(min(i+partitionSize, len(sent1)))])
    partition2.append(sent2[i:(min(i+partitionSize, len(sent2)))])
    partitionLabel.append(labels[i:(min(i+partitionSize, len(labels)))])

    i += partitionSize

for i in range(len(partition1)):
    with gzip.GzipFile('train1_'+str(i)+'.pgz', "w") as file:
        pickle.dump(partition1[i], file)
    with gzip.GzipFile('train2_'+str(i)+'.pgz', "w") as file:
        pickle.dump(partition2[i], file)
    with gzip.GzipFile('trainLabel_'+str(i)+'.pgz', "w") as file:
        pickle.dump(partitionLabel[i], file)
