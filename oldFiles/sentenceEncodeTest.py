import numpy as np
import tensorflow as tf
import pickle

grouping = pickle.load(open("words.dat","rb"))
vocabulary = grouping[0]
lookup = grouping[1]

inv_map = {v: k for k, v in vocabulary.items()}

def parse(record):
    # Don't forget to replace the shape with the actual value
    features = {
        'sentence1': tf.FixedLenFeature([300], tf.int64),
        'sentence2': tf.FixedLenFeature([300], tf.int64),
        'cosSim': tf.FixedLenFeature([1], tf.float32),
        'entailment': tf.FixedLenFeature([3], tf.float32),
        'semantic': tf.FixedLenFeature([3], tf.float32)
    }
    parsed_example = tf.parse_single_example(record, features)
    return ({'sentence1':parsed_example['sentence1'],'sentence2':parsed_example['sentence2']},
    {'cosSim':parsed_example['cosSim'],'entailmentOut':parsed_example['entailment'],'semanticOut':parsed_example['semantic']})


dataset = tf.data.TFRecordDataset('trainSentencePairs.tfrecord')
dataset = dataset.map(parse)
#dataset = dataset.repeat()
#dataset = dataset.shuffle(int(300*0.4) + 3 * 32)
#dataset = dataset.batch(32)
training_iterator = dataset.make_one_shot_iterator()
next = training_iterator.get_next()
try:
    with tf.Session() as session:
        while True:
            dic = session.run(next)
            a = dic[0]['sentence1']
            b = dic[0]['sentence2']
            c = dic[1]['cosSim']
            d = dic[1]['entailmentOut']
            e = dic[1]['semanticOut']
            sentence1 = [inv_map[a[i]] for i in range(len(a)) if a[i] != 0]
            sentence2 = [inv_map[b[i]] for i in range(len(b)) if b[i] != 0]
            print(' '.join(sentence1),' && ', ' '.join(sentence2),c,', ',d,', ',e)
except tf.errors.OutOfRangeError:
    pass

print(vocabulary)
