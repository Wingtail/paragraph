import pickle
import re
import nltk
import numpy as np
import tensorflow as tf
from transformerBlock import Transformer, Siamese
import gzip

def prep(extension):
    paragraphs = pickle.load(open("processedData/"+extension+"paragraphFile","rb"))
    queries = pickle.load(open("processedData/"+extension+"queryFile","rb"))

    grouping = pickle.load(open("words.dat", "rb"))

    vocabulary = grouping[0]
    lookuptable = grouping[1]

    paragraphMat = []
    queryVecs = []

    #print(len(paragraphs))

    transformer = Transformer(3,6,300)
    transformer.load_weights('./checkpoint/transformer_best')
    transformer.compile(optimizer=tf.train.AdamOptimizer(0.001), loss='mse', metrics=['acc'])

    maximum = 248
    batchCap = 64

    paragraphLengths = []

    maxParagraphLength = 0
    sentences = []

    # dummyTemp = 0
    for paragraph in paragraphs:
        if(len(paragraph) > 0):
            paragraphLengths.append(len(paragraph))
            sentences.extend(paragraph)
        # dummyTemp += 1
        # if(dummyTemp > 60):
        #     break

    maxParagraphLength = 50

    batchLength = [] #Max num of sentences to process before it predicts

    l = 0
    for i in range(len(paragraphLengths)):
        l += paragraphLengths[i]
        if(i < len(paragraphLengths)-1):
            if(l+paragraphLengths[i+1] > batchCap):
                batchLength.append(l)
                l = 0
        elif(i==len(paragraphLengths)-1):
            batchLength.append(l)
            l=0

    paragraphBatch = np.zeros((len(paragraphLengths), maxParagraphLength, 300))

    paragraphIndex = 0
    batchIndex = 0

    index = 0

    sentenceMat = np.zeros((batchLength[batchIndex],500,300))
    for i in range(len(sentences)):
        sentence = nltk.word_tokenize(sentences[i])
        sentence = [word for word in sentence if word in vocabulary]
        for j in range(len(sentence)):
            sentenceMat[index][j] = lookuptable[vocabulary[sentence[j]]]
        index += 1
        #print(sentenceMat.shape)
        if(index == batchLength[batchIndex]):
            print('index: ',index)
            index = 0
            batchIndex += 1
            print(batchIndex/float(len(batchLength))*100,' % done')

            slider = 0
            print('predicting')
            mat = transformer.predict(sentenceMat)
            #print(mat.shape)
            if(batchIndex < len(batchLength)):
                sentenceMat = np.zeros((batchLength[batchIndex],500,300))
            for k in range(paragraphIndex, len(paragraphLengths)):
                if(slider == batchLength[batchIndex-1]):
                    break
                #print('paragraphLength: ',paragraphLengths[k])
                paragraphBatch[k][0:paragraphLengths[k]] = mat[slider:slider+paragraphLengths[k]]
                slider += paragraphLengths[k]
                paragraphIndex += 1

    print('\a')
    print('\a')
    print('paragraphBatch completed!')

    f = gzip.GzipFile('processedData/'+extension+'paragraphBatch.npy.gz', "w")

    np.save(f, paragraphBatch)

    f.close()

    print('saved!')
    paragraphBatch = None

    maxCap = len(queries)

    queryBatch = np.zeros((maxCap,300)) #

    queryLengths = [] #words per query

    slider = 0
    index = 0
    batchLength = min(maxCap-slider,batchCap)
    wordMatBatch = np.zeros((batchLength,500,300))

    for i in range(maxCap):
        words = queries[i]

        for j in range(len(words)):
            if(words[j] in vocabulary):
                wordMatBatch[index][j] = lookuptable[vocabulary[words[j]]]
        index += 1
        if(index == batchLength):
            index = 0
            mat = transformer.predict(wordMatBatch)
            print(mat.shape)
            for k in range(len(mat)):
                queryBatch[slider] = mat[k]
                slider += 1

            print(slider/float(maxCap)*100,' % done')

            length = min(maxCap-slider, batchCap)
            if(batchLength != length and length > 0):
                batchLength = length
                wordMatBatch = np.zeros((batchLength,500,300))

    print('\a')
    print('\a')
    print('queryBatch completed!')

    f = gzip.GzipFile('processedData/'+extension+'queryBatch.npy.gz', "w")

    np.save(f, queryBatch)

    f.close()

prep('')
prep('validation_')
