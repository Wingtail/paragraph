import pickle
import numpy as np
import gzip
import tensorflow as tf
from transformerBlock import Transformer, Siamese
import nltk

transformer = Transformer(3,6,300)
transformer.load_weights('./checkpoint/transformer_best')
transformer.compile(optimizer=tf.train.AdamOptimizer(0.001), loss='mse', metrics=['acc'])

f = gzip.GzipFile('processedData/paragraphBatch.npy.gz', "r")
print('loading file')
paragraphBatch = np.load(f)
f = gzip.GzipFile('processedData/queryBatch.npy.gz', "r")
print('loading file')
queryBatch = np.load(f)

paragraphs = pickle.load(open("processedData/paragraphFile","rb"))
queries = pickle.load(open("processedData/queryFile","rb"))

grouping = pickle.load(open("words.dat", "rb"))

vocabulary = grouping[0]
lookuptable = grouping[1]

#Check if all of the vectors/matrices are non-zero

count = 0
for paragraph in paragraphs:
    paragraphMat = np.zeros((len(paragraph), 50, 300))
    for i in range(len(paragraph)):
        words = nltk.word_tokenize(paragraph[i])
        for j in range(len(words)):
            paragraphMat[i][j] = lookuptable[vocabulary[words[j]]]

    sentenceMat = transformer.predict(paragraphMat)
    if(np.array_equal(paragraphBatch[count], sentenceMat) == False):
        print('error!')
    count += 1

# paragraphBatch = np.sum(np.abs(paragraphBatch), axis = 2)
# paragraphBatch = np.sum(paragraphBatch, axis = 1)
#
# queryBatch = np.sum(np.abs(queryBatch), axis=1)
#
# for i in range(len(paragraphBatch)):
#     if(paragraphBatch[i] <= 0.0):
#         print('error!')
#         break
#
#
# for i in range(len(queryBatch)):
#     if(queryBatch[i] <= 0.0):
#         print('error!')
#         break
print('complete!')
